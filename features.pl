/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  features.pl -- 
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require('format.pl').
:-require('utils.pl').

:-extensional 'N'/1, 'C'/4, 'S'/1,'D'/2,'L'/3.

:-features( lemma, [lex,lemma,xfullcat,fullcat,cat,mstag,dict,length] ).
:-finite_set(ponct,[',','.',';','(',')','[',']','!','?',':','"']).

:-extensional tagset!label/1.
:-extensional tagset!simple_label/2.
:-extensional tagset!label2index/2.
:-extensional tagset!nlabels/1.
:-extensional tagset!action2index/5.
:-extensional tagset!index2action/3.
:-extensional tagset!templates/1.
:-extensional tagset!fullcat/2.
:-extensional tagset!cat/2.
:-extensional tagset!template_prefix/1.
:-extensional tagset!complete/0.
:-extensional tagset!actionbase2index/2.

:-extensional tagset!block/3.

%% extra actions for DAGs and non-projectivity
:-extensional
  tagset!pop0/0,			% pop the topmost stack element
  tagset!pop1/0,			% pop the second stack element
  tagset!swap/0,			% exchange the 2 topmost stack elements
  tagset!attach/0,			% the 2 attach actions for DAG (variant of the reduce actions)
  tagset!reduce/0,			% the 2 reduce actions
  tagset!reduce2/0,			% the 2 reduce2 actions for deep attach+reduce
  tagset!noop/0,			% do nothing at the end of a sentence to compensate for path lengths
  tagset!swap2/0,			% exchange the 1st and 3rd topmost elements
  tagset!ensure_tree/0,			% ensure each node has (at most) one governor
  tagset!late_attach/0,
  tagset!allow_cycle/0
	.

:-xcompiler
null_entry(Entry,V) :- Entry = lemma{ lex => V, lemma => V, cat => V, fullcat => V, xfullcat => V, dict => [], length => 0}.	

:-xcompiler
null_dep(Label,Entry,V,D,Gov,NGov) :-
	Label=0,
	V=0,
	D=[],
	Gov=[],
	NGov=0,
	null_entry(Entry,0)
	.

:-xcompiler
simple_label(L,XL) :-
	( tagset!simple_label(L,XL) -> true ; L=XL)
	.

:-xcompiler
feature_strip(V,XV) :-
    (V=[XV] xor V=XV)
	.

:-std_prolog stack_features/2.

stack_features( Tree,
		[ Left,
		  Pos,
		  Entry,
		  XBLabel,BEntry,BV,XBD,XBGov,NBGov,XBLabel2,BEntry2,
		  XALabel,AEntry,AV,XAD,XAGov,NAGov,XALabel2,AEntry2,
		  XGov,
		  NGov,
		  GovEntry
		]
	      ) :-
	( Tree = tree(Head,Before,After,Gov,GovHead) ->
	  (Head = 0 ->
	   %% virtual root node
	   Left = 0,
	   Pos = -1,
	   null_entry(Entry,0)
	  ;
	   recorded('C'(Left,
			_,
			Entry::lemma{},
			Right),
		    Head),
	   ( Left = 0 -> Pos = 0
	   ; 'N'(Right) -> Pos = 1
	   ; Pos = 2
	   )
	  ),
	  feature_strip(Gov,XGov),
	  length(Gov,NGov),
	  (GovHead = [] ->
	       null_entry(GovEntry,0)
	   ; GovHead = 0 ->
	       null_entry(GovEntry,0)
	   ;
	   recorded('C'(GovLeft,_,GovEntry::lemma{},GovRight),GovHead)
	  ),
	  ( Before = child_info(BLabel,BHead,BV,BD,BGov,BLabel2,BHead2) ->
		feature_strip(BD,XBD),
	    simple_label(BLabel,XBLabel),
	    length(BGov,NBGov),
	    feature_strip(BGov,XBGov),
	    recorded('C'(_,_,BEntry::lemma{},_),BHead),
	    (BLabel2 = 0 ->
		 XBLabel2 = 0,
		 null_entry(BEntry2,0)
	     ;
	     recorded('C'(_,_,BEntry2::lemma{},_),BHead2),
	     simple_label(BLabel2,XBLabel2)
	    )
	  ;
	    null_dep(XBLabel,BEntry,BV,XBD,XBGov,NBGov),
	    XBLabel2 = 0,
	    null_entry(BEntry2,0)
	  ),
	  ( After = child_info(ALabel,AHead,AV,AD,AGov,ALabel2,AHead2) ->
		feature_strip(AD,XAD),
	    simple_label(ALabel,XALabel),
	    length(AGov,NAGov),
	    feature_strip(AGov,XAGov),
	    recorded('C'(_,_,AEntry::lemma{},_),AHead),
	    (ALabel2 = 0 ->
		 XALabel2 = 0,
		 null_entry(AEntry2,0)
	     ;
	     recorded('C'(_,_,AEntry2::lemma{},_),AHead2),
	     simple_label(ALabel2,XALabel2)
	    )
	  ;
	    null_dep(XALabel,AEntry,AV,XAD,XAGov,NAGov),
	    XALabel2 = 0,
	    null_entry(AEntry2,0)
	  ),
	  true
	;
	  Tree = [],
	  Left = 0,
	  Pos = -1,
	  NGov=0,
	  XGov=[],
	  null_entry(Entry,0),
	  null_dep(XBLabel,BEntry,BV,XBD,XBGov,NBGov),
	  null_dep(XALabel,AEntry,AV,XAD,XAGov,NAGov),
	  XBLabel2 = 0,
	  null_entry(BEntry2,0),
	  XALabel2 = 0,
	  null_entry(AEntry2,0)
	),
	true
	.

:-xcompiler
get_word_features(I,Id,EntryI::lemma{}) :-
	( Id < 0 ->
	      null_entry(EntryI,Id)
	  ; 'N'(N), I >= N ->
		V is I-N,
		Id = 0,
		('D'(V,EntryI) xor null_entry(EntryI,0))
	  ;
	  'C'(I,Id,EntryI::lemma{},_)
	)
	.


:-xcompiler
get_xword_features(I,Form,AllCats) :-
    ( 'L'(I,Form,AllCats) xor Form=0,AllCats=0)
	.


:-std_prolog discrete_distance/2.

discrete_distance(D,XD) :-
    (D > 0 ->
	 ( D < 8 -> XD = D
	  ; D < 10 -> XD = 8
	  ; D < 14 -> XD = 10
	  ; D < 20 -> XD = 14
	  ; XD = 20
	 )
     ;
     ( D > -8 -> XD = D
      ; D > -10 -> XD = -8
      ; D > -14 -> XD = -10
      ; D > -20 -> XD = -14
      ; XD = -20
     )
    )
.

:-light_tabular ponct_features/3.
:-mode(ponct_features/3,+(+,+,-)).

ponct_features(Left,Right,XFeats) :-
    ( Right is Left + 1 -> 
	  Feats = [nocomma,noponct]
      ;
      mutable(M,[],true),
      XLeft is Left + 1,
      XRight is Right - 1,
      every((
		   term_range(XLeft,XRight,_TLeft),
		   domain(_Left,_TLeft),
		   'C'(_Left,_,lemma{ lex => _Lex::ponct[] }, _ ),
		   mutable_read(M,_Feats),
		   ( _Lex = ',' ->
			 ( domain('comma',_Feats) xor mutable_list_extend(M,comma) ),
			 ( \+ domain('mcoma',_Feats),
			   domain(_XLeft,_TLeft),
			   _XLeft > _Left,
			   'C'(_XLeft,_,lemma{ lex => ','}, _ ) ->
			       mutable_list_extend(M,mcomma)
			   ;
			   fail
			 )
		     ; _Lex = ponct['(','['] ->
			   ( domain('open',_Feats) xor mutable_list_extend(M,open) )
		     ; _Lex = ponct[']',')'] ->
			   ( domain('close',_Feats) xor mutable_list_extend(M,close) )
		     ; _Lex = ponct['.','!','?',':',';'] ->
			   ( domain('final',_Feats) xor mutable_list_extend(M,final) )
		     ; \+ domain(ponct,_Feats),
		       mutable_list_extend(M,ponct)
		   )		     
	   )),
      mutable_read(M,Feats1),
      ( domain('comma',Feats1) xor mutable_list_extend(M,nocoma) ),
      ( Feats1=[] xor mutable_list_extend(M,noponct) ),
      mutable_read(M,Feats),
      feature_strip(Feats,XFeats),
%%      format('ponct features ~w ~w => ~w\n',[Left,Right,Feats]),
      true
    )
.

:-std_prolog feature_value_simplify/2.

feature_value_simplify(L,XL) :-
    (L = [] -> XL= []
     ; L=[V|L2] ->
	   XL=[XV|XL2],
	   (V=[XV] xor V=XV),
	   feature_value_simplify(L2,XL2)
    )
.

%% Data: the list of all features
%% Should go in some resource file
:-extensional feature_map/2.

feature_map( [ Delta0,Delta1,Delta01,
	       lemma{ lex => LexI, lemma=> LemmaI, cat => CatI, xfullcat =>FullCatI, mstag => MstagI, dict => DictI, length => LI },
	       lemma{ lex => LexI2, lemma=> LemmaI2, cat => CatI2, xfullcat =>FullCatI2, mstag => MstagI2, dict => DictI2, length => LI2},
	       lemma{ lex => LexI3, lemma=> LemmaI3, cat => CatI3, xfullcat =>FullCatI3, mstag => MstagI3, dict => DictI3, length => LI3},
	       lemma{ lex => LexI4, lemma=> LemmaI4, cat => CatI4, xfullcat =>FullCatI4, mstag => MstagI4, dict => DictI4, length => LI4},
	       [ Left0, Pos0,
		 lemma{ lex => Lex0, lemma => Lemma0, cat => Cat0, xfullcat => FullCat0, mstag => Mstag0, dict => Dict0, length => L0},
		 BLabel0, lemma{ lex => BLex0, lemma => BLemma0, cat => BCat0, xfullcat => BFullCat0, dict => BDict0, length => BL0},BV0,BD0,BGov0,NBGov0,
		 B2Label0, lemma{ lex => B2Lex0, lemma => B2Lemma0, cat => B2Cat0, xfullcat => B2FullCat0, dict => B2Dict0, length => B2L0},
		 ALabel0, lemma{ lex => ALex0, lemma => ALemma0, cat => ACat0, xfullcat => AFullCat0, dict => ADict0, length => AL0},AV0,AD0,AGov0,NAGov0,
		 A2Label0, lemma{ lex => A2Lex0, lemma => A2Lemma0, cat => A2Cat0, xfullcat => A2FullCat0, dict => A2Dict0, length => A2L0},
		 Gov0,
		 NGov0,
		 lemma{ lex => GLex0, lemma => GLemma0, cat => GCat0, xfullcat => GFullCat0, dict => GDict0, length => GLength0 }
	       ],
	       [ Left1, Pos1,
		 lemma{ lex => Lex1, lemma => Lemma1, cat => Cat1, xfullcat => FullCat1, mstag => Mstag1, dict => Dict1, length => L1},
		 BLabel1, lemma{ lex => BLex1, lemma => BLemma1, cat => BCat1, xfullcat => BFullCat1, dict => BDict1, length => BL1},BV1,BD1,BGov1,NBGov1,
		 B2Label1, lemma{ lex => B2Lex1, lemma => B2Lemma1, cat => B2Cat1, xfullcat => B2FullCat1, dict => B2Dict1, length => B2L1},
		 ALabel1, lemma{ lex => ALex1, lemma => ALemma1, cat => ACat1, xfullcat => AFullCat1, dict => ADict1, length => AL1},AV1,AD1,AGov1,NAGov1,
		 A2Label1, lemma{ lex => A2Lex1, lemma => A2Lemma1, cat => A2Cat1, xfullcat => A2FullCat1, dict => A2Dict1, length => A2L1},
		 Gov1,
		 NGov1,
		 lemma{ lex => GLex1, lemma => GLemma1, cat => GCat1, xfullcat => GFullCat1, dict => GDict1, length => GLength1 }
	       ],
	       [ Left2, Pos2,
		 lemma{ lex => Lex2, lemma => Lemma2, cat => Cat2, xfullcat => FullCat2, mstag => Mstag2, dict => Dict2, length => L2},
		 BLabel2, lemma{ lex => BLex2, lemma => BLemma2, cat => BCat2, xfullcat => BFullCat2, dict => BDict2, length => BL2},BV2,BD2,BGov2,NBGov2,
		 B2Label2, lemma{ lex => B2Lex2, lemma => B2Lemma2, cat => B2Cat2, xfullcat => B2FullCat2, dict => B2Dict2, length => B2L2},
		 ALabel2, lemma{ lex => ALex2, lemma => ALemma2, cat => ACat2, xfullcat => AFullCat2, dict => ADict2, length => AL2},AV2,AD2,AGov2,NAGov2,
		 A2Label2, lemma{ lex => A2Lex2, lemma => A2Lemma2, cat => A2Cat2, xfullcat => A2FullCat2, dict => A2Dict2, length => A2L2},
		 Gov2,
		 NGov2,
		 lemma{ lex => GLex2, lemma => GLemma2, cat => GCat2, xfullcat => GFullCat2, dict => GDict2, length => GLength2 }
	       ],
	       Guide,
	       DeltaStep,
	       [ FormI5, AllCatsI5 ],
	       [ PoncFeatsI1 ],
	       [ LastBase, LastLabel ]
	     ],
	     [ delta0: Delta0,
	       delta1: Delta1,
	       delta01: Delta01,
	       
	       lexI: LexI,
	       lemmaI: LemmaI,
	       catI: CatI,
	       fullcatI: FullCatI,
	       dictI: DictI,
	       lI: LI,
	       
	       lexI2: LexI2,
	       lemmaI2: LemmaI2,
	       catI2: CatI2,
	       fullcatI2: FullCatI2,
	       dictI2: DictI2,
	       lI2: LI2,
	       
	       lexI3: LexI3,
	       lemmaI3: LemmaI3,
	       catI3: CatI3,
	       fullcatI3: FullCatI3,
	       dictI3: DictI3,
	       lI3: LI3,

	       lexI4: LexI4,
	       lemmaI4: LemmaI4,
	       catI4: CatI4,
	       fullcatI4: FullCatI4,
	       dictI4: DictI4,
	       lI4: LI4,
	       
	       lex0: Lex0,
	       lemma0: Lemma0,
	       cat0: Cat0,
	       fullcat0: FullCat0,
	       dict0: Dict0,
	       l0: L0,
	       ngov0: NGov0,
	       gov0: Gov0,
	       agov0: AGov0,
	       bgov0: BGov0,
	       nagov0: NAGov0,
	       nbgov0: NBGov0,
	       glex0: GLex0,
	       glemma0: GLemma0,
	       gcat0: GCat0,
	       gfullcat0: GFullCat0,
	       gdict0: GDict0,
	       gl0: GLength0,

	       lex1: Lex1,
	       lemma1: Lemma1,
	       cat1: Cat1,
	       fullcat1: FullCat1,
	       dict1: Dict1,
	       l1: L1,
	       ngov1: NGov1,
	       gov1: Gov1,
	       agov1: AGov1,
	       bgov1:BGov1,
	       nagov1: NAGov1,
	       nbgov1: NBGov1,
	       glex1: GLex1,
	       glemma1: GLemma1,
	       gcat1: GCat1,
	       gfullcat1: GFullCat1,
	       gdict1: GDict1,
	       gl1: GLength1,

	       lex2: Lex2,
	       lemma2: Lemma2,
	       cat2: Cat2,
	       fullcat2: FullCat2,
	       dict2: Dict2,
	       l2: L2,
	       ngov2: NGov2,
	       gov2: Gov2,
	       nagov2: NAGov2,
	       nbgov2: NBGov2,
	       glex2: GLex2,
	       glemma2: GLemma2,
	       gcat2: GCat2,
	       gfullcat2: GFullCat2,
	       gdict2: GDict2,
	       gl2: GLength2,

	       blex0: BLex0,
	       blemma0: BLemma0,
	       bcat0: BCat0,
	       bfullcat0: BFullCat0,
	       bdict0: BDict0,
	       bl0: BL0,
	       
	       alex0: ALex0,
	       alemma0: ALemma0,
	       acat0: ACat0,
	       afullcat0: AFullCat0,
	       adict0: ADict0,
	       al0: AL0,
	       
	       blex1: BLex1,
	       blemma1: BLemma1,
	       bcat1: BCat1,
	       bfullcat1: BFullCat1,
	       bdict1: BDict1,
	       bl1: BL1,
	       
	       alex1: ALex1,
	       alemma1: ALemma1,
	       acat1: ACat1,
	       afullcat1: AFullCat1,
	       adict1: ADict1,
	       al1: AL1,

	       blex2: BLex2,
	       blemma2: BLemma2,
	       bcat2: BCat2,
	       bfullcat2: BFullCat2,
	       bdict2: BDict2,
	       bl2: BL2,
	       
	       alex2: ALex2,
	       alemma2: ALemma2,
	       acat2: ACat2,
	       afullcat2: AFullCat2,
	       adict2: ADict2,
	       al2: AL2,
	       
	       blabel0: BLabel0,
	       alabel0: ALabel0,
	       
	       blabel1: BLabel1,
	       alabel1: ALabel1,

	       blabel2: BLabel2,
	       alabel2: ALabel2,

	       b2label0: B2Label0,
	       b2lex0: B2Lex0,
	       b2lemma0: B2Lemma0,
	       b2cat0: B2Cat0,
	       b2fullcat0: B2FullCat0,
	       b2dict0: B2Dict0,
	       b2l0: B2L0,
	       a2label0: A2Label0,
	       a2lex0: A2Lex0,
	       a2lemma0: A2Lemma0,
	       a2cat0: A2Cat0,
	       a2fullcat0: A2FullCat0,
	       a2dict0: A2Dict0,
	       a2l0: A2L0,

	       b2label1: B2Label1,
	       b2lex1: B2Lex1,
	       b2lemma1: B2Lemma1,
	       b2cat1: B2Cat1,
	       b2fullcat1: B2FullCat1,
	       b2dict1: B2Dict1,
	       b2l1: B2L1,
	       a2label1: A2Label1,
	       a2lex1: A2Lex1,
	       a2lemma1: A2Lemma1,
	       a2cat1: A2Cat1,
	       a2fullcat1: A2FullCat1,
	       a2dict1: A2Dict1,
	       a2l1: A2L1,

	       b2label2: B2Label2,
	       b2lex2: B2Lex2,
	       b2lemma2: B2Lemma2,
	       b2cat2: B2Cat2,
	       b2fullcat2: B2FullCat2,
	       b2dict2: B2Dict2,
	       b2l2: B2L2,
	       a2label2: A2Label2,
	       a2lex2: A2Lex2,
	       a2lemma2: A2Lemma2,
	       a2cat2: A2Cat2,
	       a2fullcat2: A2FullCat2,
	       a2dict2: A2Dict2,
	       a2l2: A2L2,

	       mstagI: MstagI,
	       mstag0: Mstag0,
	       mstag1: Mstag1,
	       mstag2: Mstag2,
	       mstagI2: MstagI2,
	       mstagI3: MstagI3,
	       mstagI4: MstagI4,

	       pos0: Pos0,
	       pos1: Pos1,
	       pos2: Pos2,

	       bv0: BV0,
	       av0: AV0,
	       bv1: BV1,
	       av1: AV1,
	       bv2: BV2,
	       av2: AV2,

	       bd0: BD0,
	       ad0: AD0,
	       bd1: BD1,
	       ad1: AD1,
	       bd2: BD2,
	       ad2: AD2,

	       lexI5: FormI5,
	       allcatsI5: AllCatsI5,

	       ponctI: PoncFeatsI1,

	       guide: Guide,

	       dstep: DeltaStep,

	       lastbase: LastBase,
	       lastlabel: LastLabel
	     
	     ]
	    )
.

:-std_prolog expand_flist_disj/4.

expand_flist_disj(FNames,Map,Values,FSet) :-
	( FNames = [FNames1|FNames2] ->
	  expand_flist(FNames1,Map,Values1,FSet),
	  expand_flist_disj(FNames2,Map,_Values2,FSet),
	  (_Values2 =.. [disj|Values2] -> true ; Values2 = _Values2),
	  (Values1 =.. [disj|_Values1] ->
	       append(_Values1,Values2,Values)
	   ;
	   Values = [Values1|Values2]
	  )
	;
	  Values = []
	)
	.

:-std_prolog append/3.

append(A,B,C) :-
    (A = [] -> C = B
     ; A = [X|A2],
       C = [X|C2],
       append(A2,B,C2)
    )
.

:-std_prolog expand_flist/4.

expand_flist(FNames,Map,Values,FSet) :-
	( FNames = true ->
	  (expand_to_symbol(FSet,Smb) xor format('*** could not expand smb ~w\n',[FSet]), fail),
	  verbose('template=~w\n',[Smb]),
	  Values = [Smb]
	; FNames = [_|_] ->
	  (expand_flist_disj(FNames,Map,Values1,FSet) xor format('*** could not expand flist ~w\n',[FNames]), fail),
	  ( Values1 = [Values]
	   -> true
	   ; Values1 =.. [disj|_] ->
		 Values = Values1
	  ;
	    Values =.. [disj|Values1]
	  )
	; FNames = (F:FNames2) ->
	      (expand_flist(FNames2,Map,Values2,[F|FSet]) xor format('*** could not expand flist2 ~w\n',[FNames2]), fail),
	      ( domain(F:V,Map) ->
		    (recorded(feature2index(F,Index)) xor
			     update_counter(feature_ctr,Index),
		     record(feature2index(F,Index))
		    ),
		    Values = [(Index,V)|Values2]
%%	      Values = [V|Values2]
	  ;
	    format('*** warning: feature ~w not found in map\n',[F]),
	    fail
	  )
	;
	  format('*** bad format ~w\n',[FNames]),
	  fail
	)
	.

:-std_prolog expand_to_symbol/2.

expand_to_symbol(FNames,Smb) :-
	( FNames = [Smb] -> true
	; FNames = [F|FNames2],
	  expand_to_symbol(FNames2,Smb2),
	  name_builder('~w_~w',[Smb2,F],Smb)
	)
	.

:-std_prolog template_add/4.

template_add(F,T,Templates,XTemplates)	:-
%	format('try add F=~w T=~w to ~w\n',[F,T,Templates]),
	( F = (F1,F2) ->
	  template_add(F1,T,Templates,XTemplates1),
	  template_add(F2,T,XTemplates1,XTemplates)
	; Templates = [] ->
	  XTemplates = [F:XT],
	  template_factorize([T],XT)
	; Templates = [true|Templates2] ->
	  template_add(F,T,Templates2,XTemplates2),
	  XTemplates = [true|XTemplates2]
	; Templates = [F1:T1|Templates2],
	  ( F = F1 ->
	    ( T = true ->
	      ( domain(T,T1) ->
		XTemplates = Templates
	      ;
		XTemplates = [F1:[true|T1]|Templates2]
	      )
	    ;
	      ( T=XF:XT ->
		template_add(XF,XT,T1,XT1)
	      ;
		template_add(T,true,T1,XT1)
	      ),
	      XTemplates=[F1:XT1|Templates2]
	    )
	  ;
	    template_add(F,T,Templates2,XTemplates2),
	    XTemplates = [F1:T1|XTemplates2]
	  )
	),
%	format('added F=~w T=~w to ~w => ~w\n',[F,T,Templates,XTemplates]),
	true
	.

:-std_prolog template_factorize/2.

template_factorize(Templates,XTemplates) :-
%	format('try factorize ~w\n',[Templates]),
	( Templates = [T|Templates2] ->
	  template_factorize(Templates2,XTemplates2),
	  ( T=true ->
	    ( domain(true,XTemplates2) -> XTemplates = XTemplates2
	    ; XTemplates = [true|XTemplates2]
	    )
	  ; T = F:T2 ->
	    template_add(F,T2,XTemplates2,XTemplates)
	  ;
	    template_add(T,true,XTemplates2,XTemplates)
	  )
	; Templates = [],
	  XTemplates = []
	),
%	format('factorized into ~w\n',[XTemplates]),
	true
	.

:-std_prolog template_analyze/1.

template_analyze(T) :-
	( T= true -> true
	; T=F:T2 ->
	  update_counter(feature(F),_),
	  template_analyze(T2)
	;
	  update_counter(feature(T),_)
	)
	.

:-std_prolog template_complete_add/3.

template_complete_add(F,TmpTemplates,CTemplates) :-
	( TmpTemplates = [] -> CTemplates = []
	; TmpTemplates = [T|TmpTemplates2] ->
	  template_complete_add(F,TmpTemplates2,CTemplates2),
	  CTemplates = [F:T,T|CTemplates2]
	)
	.
		      
:-std_prolog template_complete/2.

template_complete(T,CTemplates) :-
	( T=true -> CTemplates = [true]
	; T = F1:T2 ->
	  template_complete(T2,CTemplates2),
	  template_complete_add(F1,CTemplates2,CTemplates) 
	;
	  CTemplates = [T]
	)
	.

:-std_prolog template_add/3.

template_add(Add,Templates,XTemplates) :-
	( Add = [] -> XTemplates = Templates
	; Add = [T1|Add2] ->
	  template_add(Add2,Templates,XTemplates2),
	  (domain(T1,XTemplates2) -> XTemplates = XTemplates2 ; XTemplates = [T1|XTemplates2])
	)
	.

:-std_prolog template_reorder/2.

template_reorder(Templates,OTemplates) :-
	(Templates = [] -> OTemplates = []
	; Templates = [T|Templates2],
	 template_reorder(Templates2,OTemplates2),
	 template_reorder_aux(T,OT),
	 template_complete(OT,CTemplates),
	 template_add(CTemplates,OTemplates2,OTemplates)
	).

:-std_prolog template_reorder_aux/2.

template_reorder_aux(T,OT) :-
	( T=true -> OT=true
	; T=F:T2 ->
	  template_reorder_aux(T2,OT2),
	  template_reorder_add(F,OT2,OT),
	  true
	;
	  OT = T
	)
	.

:-std_prolog template_reorder_add/3.

template_reorder_add(F,OT1,OT2) :-
	( OT1 = true -> OT2 = F
	; OT1 = F1:XOT1 ->
	  value_counter(feature(F),FC),
	  value_counter(feature(F1),F1C),
	  ( FC > F1C ->
	    OT2 = F:OT1
	  ; template_reorder_add(F,XOT1,XOT2),
	    OT2 = F1:XOT2
	  )
	; OT1 = F1,
	  value_counter(feature(F),FC),
	  value_counter(feature(F1),F1C),
	  ( FC > F1C ->
	    OT2 = F:F1
	  ; 
	    OT2 = F1:F
	  )
	)
	.
	  
:-std_prolog generate_templates.

generate_templates :-
	feature_map(Feats,Map),
	verbose('handling templates\n',[]),
	tagset!templates(Templates),
	length(Templates,NTemplates),
	verbose('compiling #templates=~w\n',[NTemplates]),
	(tagset!complete -> 
	 every(( domain(_Template,Templates),
		 template_analyze(_Template)
	       )),
	 template_reorder(Templates,OTemplates)
	;
	 OTemplates = Templates
	),
	%%	format('try factorize template ~w\n',[OTemplates]),
	template_factorize(OTemplates,_XTemplates),
	mutable(M,_XTemplates,true),
	every(( tagset!template_prefix(Prefix),
		mutable_read(M,_XT),
		mutable(M,[Prefix: _XT])
	      )),
	mutable_read(M,XTemplates),
	verbose('factorized templates ~w\n',[XTemplates]),
	expand_flist(XTemplates,Map,Values,[]),
	verbose('expand tree template ~w => values=~w\n',[Templates,Values]),
	record_without_doublon(templates(Feats,Values)),
	true
	.


:-xcompiler
fset_value_weight_update(Action,Label,Values,Update,NUpdates) :-
	( Action = none, NUpdates = 0
	xor
	'$interface'('FSet_Value_Weight_Update'(Action:term,Label:term,Values:term,Update:int),[return(NUpdates:int)]),
%	format('fset value update fset=~w value=~w new=~w\n',[FSet,Values,W]),
	true
	)
	.

:-xcompiler
fset_value_weight(Action,Label,Values,W,Mask) :-
%	format('try fset value fset=~w value=~w\n',[FSet,Values]),
	( var(Values) ->
	  format('unexpected variable as values ~w action=~w label=~w\n',[Values,Action,Label])
	;
%	  '$interface'('FSet_Value_Weight'(Action:term,Label:term,Values:term,W: -int,Mask:int),[choice_size(1)])
	  '$interface'('FSet_Value_Weight'(Action:term,Label:term,Values:term,W:term,Mask:int),[choice_size(1)])
	)
	.

%:-std_prolog load_model/1.
:-light_tabular load_model/1.

load_model(Model) :-
	verbose('loading model from ~w\n',[Model]),
	'$interface'('model_load'(Model:string),[return(none)]),
	true
	.

:-std_prolog save_model/1.

save_model :-
	opt(save_model(Model)),
	verbose('saving model in ~w\n',[Model]),
	'$interface'('model_save'(Model:string),[return(none)])
	.


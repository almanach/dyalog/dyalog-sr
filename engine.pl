/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2013, 2014, 2015, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  engine.pl -- Engine for Tabular Shift-Reduce Dependency Parsers
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require('format.pl').
:-require('utils.pl').
:-require('features.pl').
:-require('automata.pl').

:-extensional info!comment/1.
:-extensional info!contracted/3.
:-extensional info!ellipse/3.

% info provided by the lexer through dict files
% exploited by features *dict*
:-extensional dict!info/2. 	


?- verbose('new round\n',[]),
   show_time(latency),
   verbose('here1\n',[]),
%%   test_float(3.14159),
   process_options,
   verbose('here2\n',[]),
   ( oracle!end ->
     save_model,
     show_time(model_save)
   ;
     show_time(model_load),
     parse
   ),
   fail
   .

:-std_prolog parse/0.

parse :-
    'N'(N),
    verbose('processing new sentence N=~w\n',[N]),
	( opt(train:_) ->
	  %% we only advance the perceptron in training mode
	  perceptron!advance
	;
	  opt(check) xor  abolish(oracle!action/2)
	),
	('S'(SId) xor SId='E1'),
	%%	MaxStep is (2 * N) - 1,
	(avgweight(AvgWeight,_,__MaxSteps) xor
         format('%% *** missing avgweight\n',[]),
	 record(avgweight(700,0,0)),
	 AvgWeight=700,
	 __MaxSteps = 0
	),
	UpdateMarge is min(120.0,AvgWeight / 5),
%	Marge2 is AvgWeight/5,
%	record_without_doublon(marge(Marge2)),
	(opt(train) ->
	     format('%% marge=~w avg=~w nstep=~w\n',[UpdateMarge,AvgWeight,__MaxSteps])
	 ;
	 true
	),
	(opt(train:_),
	 \+ tagset!noop ->
	     oracle!action(MaxStep,_),
	     \+ (oracle!action(_MaxStep,_), _MaxStep > MaxStep)
	 ; maxstep_factor(MaxStepFactor) ->
%	       format('using maxstep factor ~w\n',[MaxStepFactor]),
	       MaxStep is round(MaxStepFactor * N)
	 ; tagset!attach ->
	       %% when accepting multi-governors, it is less clear how many steps we need
	       %% it would be even worse if accepting multiple edges between same nodes
	       %% the current value (3) is hardcoded and seems enough for SemEval PCEDT
	       %% but it should be a parameter or be automatically found
	       MaxStep is (4 * N)
	 ;
	 MaxStep is (2 * N)
	),
%%	beam(Beam),
%%	'$interface'('Beam_Cells_Allocate'(MaxStep:int,Beam:int),[return(none)]),
%%	format('start parsing sid=~w N=~w MaxStep=~w\n',[SId,N,MaxStep]),
	(
%%	 \+ oracle!skip_sentence,
%%	 \+ oracle!notproj(_,_),
	 beam!new_cell(0,_),
	 ( oracle!action(0,init) ->
	   record_without_doublon(oracle!item(0,InitItem))
	 ;
	   true
	 ),
	 register_item(
		       InitItem::item{ step => 0,
				       generation => 0,
				       right => 0,
				       left => 0,
				       stack => tree(0,[],[],[],[]),
				       stack1 => [],
				       prefix => 0,
				       inside => 0,
				       lk1 => 0,
				       lk2 => 0,
				       lk3 => 0,
				       action => init,
				       swap => []
				     },
		       _
		      ),
	 fail
	;
	term_range(0,MaxStep,TStep),
	domain(Step,TStep),
	
	 update_counter(generation,_),
	 value_counter(generation,CurrentGen),

	 verbose('Dealing step=~w generation=~w\n',[Step,CurrentGen]),
	 every((
		      erase(oracle!violation(Step,_,_)),
		      erase(oracle!agree(Step,_,_))
	      )),
	 ( recorded(terminal_step(_)) ->
	       fail
	   ; tagset!noop,
	   \+ ( (beam!enumerate(Step,_Item) ; oracle!item(Step,_Item)),
		\+ item!terminal(N,_Item)
	      ) ->
%	       format('register terminal step ~w\n',[Step]),
	       record_without_doublon(terminal_step(Step)),
	       fail
	   ;
	   true
	 ),
	 ( beam!best(Step,_BestItem) ->
%	       recorded(_BestItem,_BestItemAddr),
%	       (item!back(_BestItemAddr,_BestAction,_BestItemAddr0,_,_BestCost) xor true),
%	       format('**** step ~w bestaction=~w cost=~w item0=~w item=~w:~w\n',[Step,_BestAction,_BestCost,_BestItemAddr0,_BestItemAddr,_BestItem]),
	       true
	   ;
	       format('no item at step ~w\n',[Step])
	 ),
	 NStep is Step+1,
	 beam!new_cell(NStep,_),
/*
	 on_verbose(
		 every(( format('show oracle item at step=~w\n',[Step]),
			 oracle!item(Step,_Item),
			 recorded(_Item,_Addr),
			 format('oracle step=~w item=~w: ~w\n',[Step,_Addr,_Item]),
			 every(( item!tail(_Addr,_Addr2,_W),
				 format('\ttail ~w w=~w\n',[_Addr2,_W])
				 ;
				 oracle!tail(_Addr,_Addr2,_W),
				 format('\toracle tail ~w w=~w\n',[_Addr2,_W])
			      ))
		      ))
	     ),
*/
	 every((
%		      NStep is Step+1,
		(
		  (opt(train:_),
		   oracle!item(Step,Item),
		   verbose('step ~w oracle item ~w\n',[Step,Item]),
		   ( item!terminal(N,Item) ->
			 record_without_doublon(oracle!action(NStep,noop))
		     ;
		     true
		   ),
		   ( recorded(Item,_) 
			     xor format('oracle item declared but erased at step=~w item=~\n',[Step,Item])
		   ),
		   ( oracle!item(Step,AltItem),
                     \+ AltItem  = Item ->
			 format('more than one oracle at step=~w item=~w altitem=~w\n',[Step,Item,AltItem])
		     ;
		     true
		   )
	           xor 
		   \+ opt(check),
		   oracle!action(Step,PrevOracleAction),
		   format('no oracle item step=~w prevaction=~w\n',[Step,PrevOracleAction]),
		   fail
		  ),
%		 oracle!item(Step,Item),
		  \+ beam!enumerate(Step,Item),
		  oracle!action(NStep,Action),
		  verbose('using selected oracle step=~w item=~w\n',[Step,Item]),
		  recorded(Item,ItemAddr)
		 ;
		beam!enumerate(Step,Item),
		recorded(Item,ItemAddr),
		record(beam!inside(ItemAddr,Step))
		),
		
%		item!step(Step,Item,ItemAddr),
		verbose('step ~w process item ~w: ~w\n',[Step,ItemAddr,Item]),
		every((
			     generate_shift(Item,ItemAddr,Action)
			  ;
			  generate_other(Item,ItemAddr,Action)
			 ))
		  )),
	 %% Check mode (when activated)
	 every(( opt(check),
		 check_info(Step)
	      )),
	 %% update as soon as possible
	 %% may have an impact for the rest of the sentence
	 ( once((
		     %% update should be done only once for each step
		     %% furthermore, an update may erase some items
		     %% leading to segmentation errors when backtracking on recorded(...)
		     %% (note: this a bug of DyALog: deleting elements in the table while iterating on it)
		     %% to avoid the problem, we delay the erasing of the items, until
		     %% the very end of the step (and first mark the items to be deleted)
		opt(train:early),
		verbose('train at step ~w\n',[Step]),
		(oracle!item(NStep,NextOracleItem) 
		       xor 
		       (oracle!action(NStep,Action) xor true),
		 oracle!item(Step,Item0),
		 format('step ~w training missing oracle item action=~w item0=~w\n',[NStep,Action,Item0]), 
		 fail),
		recorded(NextOracleItem,NextOracleItemAddr),
		once(( item!back(NextOracleItemAddr,Action,ItemAddr0,_,NextOracleW),
		       safe_recorded(Item0::item{ step => Step, prefix => W0} ,ItemAddr0),
		       oracle!item(Step,Item0)
		    )),
		oracle!get_best_action(NStep,BestOracleAction,_,BestOracleItem),
%		verbose('step =~w oracle_action=~w best_oracle_action=~w item0=~w\n',[NStep,Action,BestOracleAction,ItemAddr0]),
		( \+ Action = BestOracleAction ->
		      %% the best item reached from the previous oracle item
		      %% is not reached through the oracle action
		      %%	   format('here oracle_item=~w\n',[NextOracleItem]),
		      update_counter(updates,_),
		      update_counter(updates1,_),
		      update_items(BestOracleItem::item{ action => BestOracleAction },
				   NextOracleItem::item{ action => Action },
				   NextOracleItemAddr,
				   Item0,
				   Item0 )
		  ;
		  beam!best(NStep,BestItem::item{ prefix => BestW }),
		  BestW > NextOracleW,
		  oracle!representative_item(NextOracleItem,RepNextOracleItem::item{ prefix => RepNextOracleW }),
%%		  RepNextOracleItem::item{ prefix => RepNextOracleW } = NextOracleItem,
		  \+ BestItem = RepNextOracleItem,
		  recorded(BestItem,BestItemAddr),
		  once((
			      item!back(BestItemAddr,BestAction,BestItemAddr0,_,BestW),
                              \+ ( % should not need this
				     item!back(BestItemAddr,_,_,_,_BestW),
				     _BestW > BestW
				 ),
			      safe_recorded(BestItem0::item{ step => Step},BestItemAddr0)
		      )),
		  (
                   %% the oracle item is not in the beam 
                   \+ beam!enumerate(NStep,RepNextOracleItem)
	           xor
		   %% the cost for the oracle item is too low wrt the cost of best item
		   RepNextOracleW < BestW - UpdateMarge
		   xor 
		   %% BestItem is a final item, but not the one predicted by the oracle !
		   BestItem = item{ right => N, left => 0, stack => tree(0,[],_,_,_), stack1 => [], lk1 => 0, lk3 => Id3, swap=> []},
                   \+ RepNextOracleItem = BestItem
		   xor
		   %% NextOracleItem is a final item, but not the best one
		   RepNextOracleItem = item{ right => N, left => 0, stack => tree(0,[],_,_,_), stack1 => [], lk1 => 0, lk3 => Id3, swap => []},
		   \+ RepNextOracleItem = BestItem
		   xor 
		   %% the best item clearly violates the oracle
		   oracle!violation(NStep,BestItem,_),
		   update_counter(updates2_violation,_)
		  ) ->
		  %% we try to compensate negatively for the best item and positively for the oracle item
		  update_counter(updates,_),
		  update_counter(updates2,_),
		  recorded(RepNextOracleItem,RepNextOracleItemAddr),
		  once(( item!back(RepNextOracleItemAddr,Action,RepItemAddr0,_,RepNextOracleW),
			 safe_recorded(RepItem0::item{ step => Step } , RepItemAddr0),
			 true
		      )),
		  verbose('update2 oitem0=~w aitem0=~w oaction=~w baction=~w\n',[ItemAddr0,BestItemAddr0,Action,BestAction]),
		  update_items(BestItem::item{ action => BestAction },
			       RepNextOracleItem::item{ action => Action },
			       RepNextOracleItemAddr,
			       BestItem0,
			       RepItem0
			      )
		  ;
%		  fail,
		  %% from a parent item, we derive a violation item (wrt oracle) with better cost than an agree item
		  %% we decide to update
		  %% the gain seems to be marginal !
		  oracle!agree(NStep,AItem::item{},ACost),
		  oracle!violation(NStep,VItem::item{},VCost),
		  beam!enumerate(NStep,VItem),
		  VCost > ACost,
%		  \+ beam!enumerate(NStep,AItem),
		  recorded(VItem,VItemAddr),
		  once((
			      item!back(VItemAddr,VAction,VItemAddr0,_,VW),
			      \+ ( % should not need this
				  item!back(VItemAddr,_,_,_,_VW),
				  _VW > VW
			      ),
			      safe_recorded(VItem0::item{ step => Step},VItemAddr0)
		      )),
		  recorded(AItem,AItemAddr),
		  once((
			      item!back(AItemAddr,AAction,AItemAddr0,_,AW),
			      \+ ( % should not need this
				  item!back(AItemAddr,_,_,_,_AW),
				  _AW > AW
			      ),
			      safe_recorded(AItem0::item{ step => Step},AItemAddr0)
		      )),
		  AItem0=VItem0,
		  true ->
		  update_counter(updates,_),
		  update_counter(updates3,_),
		  verbose('update3 aitem0=~w vitem0=~w aaction=~w vaction=~w acost=~w vcost=~w\n',[AItemAddr0,VItemAddr0,AAction,VAction,ACost,VCost]),
		  update_items(VItem::item{ action => VAction},
			       AItem::item{ action => AAction},
			       AItemAddr,
			       VItem0,
			       AItem0
			      )
		  ; 
		  fail,
		  beam!enumerate(S,NextOracleItem),
		  beam!enumerate(S,BetterItem::item{ prefix => BetterW }),
		  BetterW > NextOracleW,
		  recorded(BetterItem,BetterItemAddr),
		  once((
			      item!back(BetterItemAddr,BetterAction,BetterItemAddr0,_,BetterW),
			      \+ ( % should not need this
				  item!back(BetterItemAddr,_,_,_,_BetterW),
				  _BetterW > BetterW
			      ),
			      safe_recorded(BetterItem0::item{ step => Step, prefix => BetterW0 },BetterItemAddr0)
		      )),
		  BetterW < W0 ->
		  update_counter(updates,_),
		  update_counter(updates4,_),
		  verbose('update4 oitem0=~w aitem0=~w oaction=~w baction=~w\n',[ItemAddr0,BetterItemAddr0,Action,BetterAction]),
		  update_items(BetterItem::item{ action => BetterAction },
			       NextOracleItem::item{ action => Action },
			       NextOracleItemAddr,
			       BetterItem0,
			       Item0
			      )
		  ;
                  \+ tagset!noop,
		  beam!best(NStep,NextOracleItem),
		  item!terminal(N,NextOracleItem),
%		  format('potential update5-1 N=~w oraclew=~w avgweight=~w\n',[N,NextOracleW,AvgWeight]),
		  ( beam!enumerate(NStep,NonTerminalItem::item{ right => N1, prefix => NTIW }),
		    \+ item!terminal(N,NonTerminalItem),
%		    DeltaS is 2.0*(1+N-N1)*AvgWeight,
		    Bound is NTIW + 2.0 * (1+N-N1) * AvgWeight,
%		    format('\tpotential update5-1-1 oraclew=~w N1=~w NTIW=~w Bound=~w deltaS=~w\n',[NextOracleW,N1,NTIW,Bound,DeltaS]),
		    NextOracleW <  Bound ->
			true
		    ;
		    fail
		  ) ->
		      recorded(NonTerminalItem,NTIAddr),
%		      format('potential update5-2 nitw=~w\n',[NTIW]),
		      once((
				  item!back(NTIAddr,NTIAction,NTIAddr0,_,NTIW),
                            \+ ( % should not need this
				  item!back(NTIAddr,_,_,_,_NTIW),
				  _NTIW > NTIW
			      ),
			      safe_recorded(NTI0::item{ step => Step, prefix =>NTIW0 },NTIAddr0)
		      )),

		      update_counter(updates,_),
		      update_counter(updates5,_),
		      format('update5 N=~w N1=~w\n',[N,N1]),
		      update_items(NonTerminalItem::item{ action => NTIAction },
				   NextOracleItem::item{ action => Action },
				   NextOracleItemAddr,
				   NTI0::item{},
				   Item0::item{}
				  )
		  ;
		  fail
		),
		true
	       ))
	     ,
	   fail
	   ;
	   verbose('cleaning step=~w\n',[Step]),
	   every((  ( recorded(item!delete(_ItemAddr)),
		      verbose('try erase item ~w\n',[_ItemAddr]),
                      \+ ( recorded(_Item::item{ step => _S },_ItemAddr),
			   oracle!item(_S,_Item),
			   % verbose('cancel erasing of oracle item ~w: ~w\n',[_ItemAddr,_Item]),
			   true
			 )
		      ;
		      recorded(item!delete_generation(NStep,CurrentGen)),
		      recorded(item{ step => NStep, generation => CurrentGen }, _ItemAddr),
		      verbose('delete step ~w generation ~w item ~w\n',[NStep,CurrentGen,_ItemAddr]),
		      true
		    ),
		    delete_address(_ItemAddr),
		    verbose('erase item ~w\n',[_ItemAddr]),
		    update_counter(erasing,_),
		   %	  erase(item!stack(ItemAddr,_)),
		    erase(item!tails(_ItemAddr,_)),
		    erase(item!back(_ItemAddr,_,_,_,_)),
		    erase(oracle!tail(_ItemAddr,_,_)),
		    verbose('done erase item ~w\n',[_ItemAddr]),
		    verbose('done erase2 item ~w\n',[_ItemAddr]),
		    true
		)),
	   verbose('done cleaning step=~w\n',[Step]),
	   abolish(item!delete/1),
	   abolish(item!delete_generation/2),
	   abolish(update_processed/1),
	   fail
	 )
	;
	   true
	),
%	_MaxStep=MaxStep,
	verbose('completed all steps\n',[]),
	( tagset!noop ->
		 (recorded(terminal_step(_MaxStep)) xor _MaxStep = MaxStep),
		 __MaxStep = _MaxStep,
		 verbose('retrieve terminal step ~w\n',[_MaxStep]),
		 true
	 ;
	     true
	),
	( (beam!enumerate(_MaxStep,
		     FinalItem::item{ step => _MaxStep,
				      right => N,
				      left => 0,
				      stack => tree(0,[],_,_,_),
				      stack1 => [],
				      prefix => Cost,
				      inside => _Cost,
				      lk1 => 0,
				      lk2 => 0,
					  %				      lk3 => 0,
					  swap => []
				   }
		    )
	   ; oracle!item(_MaxStep,FinalItem)
	   %% ; beam!enumerate(_MaxStep,FinalItem),
	   %%   format('*** step=~w final in beam but not the best !\n',[_MaxStep]),
	   %%   true
	  ),
%         verbose('potential final item step=~w ~w\n',[_MaxStep,FinalItem]),
	  \+ (
	      %% with lattice, we may have final items for various values of step
	      %% we keep the best one
	      beam!enumerate(__MaxStep,_BetterItem::item{ right => N, stack => tree(0,[],_,_,_), stack1 => [], lk1 => 0, prefix => _BetterCost, swap => [] }),
	      _BetterCost > Cost,
	      %% format('\tbetter item ~w\n',[_BetterItem]),
	      true
	  )
	->
	  format('## ~w cost=~w inside=~w step=~w\n',[SId,Cost,_Cost,_MaxStep]),
	  %% every((
	  %% 	       beam!enumerate(_,_BetterItem),
	  %% 	       _BetterCost > Cost,
	  %% 	       format('** could have chosen better cost ~w ~w\n',[_BetterCost,_BetterItem]),
	  %% 	       true
	  %%      )),
	  every((info!comment(Comment),
		 format('##. ~w\n',[Comment])
		)),
	  (recorded(FinalItem,FinalItemAddr) xor fail), % strange, but sometines several identical final items
	  recorded(InitItem,InitItemAddr),
	  item!tree(FinalItemAddr,0),
	  every(( tree!display(0) )),
	  format('\n',[]),
	  every(( recorded(avgweight(_Avg,_AllCost,_AllStep),_AvgAddr) ->
		      _NewAllCost is _AllCost+abs(Cost),
		      _NewAllStep is _AllStep+_MaxStep,
		      _NewAvg is (_NewAllCost / _NewAllStep),
%		     format('new avg is ~w allcost=~w allstep=~w\n',[_NewAvg,_NewAllCost,_NewAllStep]),
		     %delete_address(_AvgAddr),
		      erase(avgweight(_,_,_)),
		      record(avgweight(_NewAvg,_NewAllCost,_NewAllStep)),
		      true
		  ;
		  fail
	       ))
	  ; tagset!pop0,
	    beam!best( MaxStep,
		       PartialBestItem::item{}
		     ) ->
		verbose('try complete with pop0 ations\n',[]),
		(complete_item(PartialBestItem,_FinalItem::item{ prefix => Cost, inside => _Cost }) xor fail),
		format('## ~w partial cost=~w inside=~w step=~w\n',[SId,Cost,_Cost,_MaxStep]),
		recorded(_FinalItem,_FinalItemAddr),
		recorded(InitItem,InitItemAddr),
		item!tree(_FinalItemAddr,0),
		every(( tree!display(0) )),
		format('\n',[])
	;
	  format('## ~w failure\n',[SId,Cost,_Cost])
	),
	persistent!add_fact(avgweight(_,_,_)),
	show_time(parsing),
	value_counter(checking,Checks),
	value_counter(adding,Adding),
	value_counter(erasing,Erasing),
	value_counter(updates,Updates),
	value_counter(updates1,Updates1),
	value_counter(updates2,Updates2),
	value_counter(updates2_violation,Updates2_Violation),
	value_counter(updates3,Updates3),
	value_counter(updates4,Updates4),
	format('stats checking=~w adding=~w erasing=~w updates=~w u1=~w u2=~w u3=~w u4=~w u2v=~w\n',[Checks,Adding,Erasing,Updates,Updates1,Updates2,Updates3,Updates4,Updates2_Violation]),
	fail
	.


:-std_prolog parse_options/1.

parse_options(Options) :-
        ( Options == [] -> true
	;
	  ( Options = ['-verbose'|Rest] -> record_without_doublon( opt(verbose) )
	  ; Options = ['-multi'|Rest] -> record_without_doublon( opt(multi) )
	  ; Options = ['-res',File|Rest] -> read_dbfile(File)
	  ; Options = ['-train',Mode::train[]|Rest] ->
	    %% Training modes in {early,late,best}
	    erase(opt(train:_)),
	    record_without_doublon(opt(train:Mode))
	  ; Options = ['-train'|Rest] ->
	    %% default training mode
	    erase(opt(train:_)),
	    record_without_doublon(opt(train:early))
	  ; Options = ['-load_model',Model|Rest] ->
	    record_without_doublon(opt(load_model(Model))),
	    load_model(Model),
	    true
	  ; Options = ['-save_model',Model|Rest] ->
	    %% -save_model option implies -train option
	    record_without_doublon(opt(save_model(Model))),
	    %% record_without_doublon(opt(train)),
	    true
	  ; Options = ['-beam',XN|Rest] ->
	    atom_number(XN,N),
	    record_without_doublon(beam(N))
	  ; Options = ['-sbeam',XN|Rest] ->
	    atom_number(XN,N),
	    record_without_doublon(sbeam(N))
	  ; Options = ['-mode',Mode|Rest] ->
	    %% alias for -train
	    erase(opt(train:_)),
	    record_without_doublon(opt(train:Mode))
	  ; Options = ['-mstag'|Rest] ->
		record_without_doublon(opt(mstag))
	  ; Options = ['-maxstep',XN|Rest] ->
		atom_number(XN,N),
		record_without_doublon(maxstep_factor(N))
	  ; Options = ['-check'|Rest] ->
		%% check wrt oracle, but don't train
		record_without_doublon(opt(check))
	  ; Options = [ParserOption|Rest] ->
	    true
	  ;  fail
	  ),
	  parse_options(Rest)
        )
.

:-xcompiler
register_simple_action(MSimple,Action) :-
    atom_module(Directive,tagset,Action),
    recorded(Directive),
    mutable_inc(MSimple,_Action),
    verbose('register ~w action ~w\n',[Action,_Action]),
    record(tagset!action2index(Action,0,0,_Action,0)),
    record(tagset!index2action(0,Action,0)),
    record(tagset!index2action(_Action,Action,0))
.
	       

:-xcompiler
process_options :-	
	( recorded(parsed_options)
	xor
	argv(Options),
	  parse_options(Options),
	  format('parsed options\n',[]),
	  (beam(Beam) xor record(beam(1)), Beam=1),
	  %% SBeam = selection beam indicates the max number of actions to consider from one item
	  %% by default, its value is Beam
	  %% but we can use SBeam to simulate a kind of Selectional Branching (see Choi & McCallum)
	  (sbeam(SBeam) xor SBeam = Beam, record(sbeam(SBeam))),
	  (SBeam > Beam -> XBeam = Beam ; XBeam = SBeam ),
	  (maxstep_factor(_) -> true
	   ; recorded(tagset!maxstep(MaxStepFactor)) -> record(maxstep_factor(MaxStepFactor))
	   ; record(maxstep_factor(2))
	  ),
	  generate_templates,
	  value_counter(labels,NLabels),
	  record(tagset!nlabels(NLabels)),
	  %% registering simple actions 
	  mutable(MSimple,1,true),
	  %% 0=shift
	  record(tagset!action2index(shift,0,0,0,0)),
	  record(tagset!index2action(0,shift,0)),
	  every(( domain(SimpleAction,simple_action[pop0,pop1,swap,swap2,noop]),
		  register_simple_action(MSimple,SimpleAction)
		)),
	  mutable_read(MSimple,NSimple),
	  mutable(MComplex,2,true),
	  %% reduce actions
	  LeftBase = NSimple,
	  RightBase is NSimple + 1,
	  record(tagset!actionbase2index(left,LeftBase)),
	  record(tagset!actionbase2index(right,RightBase)),
	  every(( tagset!label2index(Label,I),
		  LI is NSimple + I -1,
		  RI is LI + NLabels,
		  record(tagset!action2index(left,Label,LI,LeftBase,I)),
		  record(tagset!index2action(LI,left,Label)),
		  record(tagset!action2index(right,Label,RI,RightBase,I)),
		  record(tagset!index2action(RI,right,Label)),
		  every((domain(_Action,[shift,reduce_right,potential_reduce_right,reduce_left]),
                         name_builder('~w_~w',[_Action,Label],_GuidingAction),
			 record(guide!xaction(_Action,Label,_GuidingAction)),
			 true
		       )),
		  true
		)),
	  ( tagset!attach ->
		mutable_add(MComplex,2),
		LeftABase is NSimple + 2,
		RightABase is NSimple + 3,
		record(tagset!actionbase2index(lefta,LeftABase)),
		record(tagset!actionbase2index(righta,RightABase)),
	        every(( tagset!label2index(Label,I),
			LI is NSimple + 2*NLabels + I -1,
			RI is LI + NLabels,
			record(tagset!action2index(lefta,Label,LI,LeftABase,I)),
			record(tagset!index2action(LI,lefta,Label)),
			record(tagset!action2index(righta,Label,RI,RightABase,I)),
			record(tagset!index2action(RI,righta,Label))
		     ))
	   ; 
	   record_without_doublon(tagset!reduce),
	   true
	  ),
	  ( tagset!reduce2 ->
		mutable_add(MComplex,2),
		mutable_read(MComplex,_NComplex),
		LeftBase2 is NSimple + _NComplex - 2,
		RightBase2 is LeftBase2 + 1,
		record(tagset!actionbase2index(left2,LeftBase2)),
		record(tagset!actionbase2index(right2,RightBase2)),
	        every(( tagset!label2index(Label,I),
			%			LI is NSimple + LeftBase2*NLabels + I -1,
			LI is NSimple + (_NComplex-2)*NLabels + I -1,
			RI is LI + NLabels,
			record(tagset!action2index(left2,Label,LI,LeftBase2,I)),
			record(tagset!index2action(LI,left2,Label)),
			record(tagset!action2index(right2,Label,RI,RightBase2,I)),
			record(tagset!index2action(RI,right2,Label))
		     ))
	   ; 
	   true
	  ),
	  mutable_read(MComplex,NComplex),
	  MaxActions is NComplex*NLabels+NSimple,
	  '$interface'(set_max_action(MaxActions:int, NLabels:int, NSimple:int, NComplex: int, XBeam: int),[return(none)]),
	  record(tagset!label2index(0,0)),
%	  every(( _A::tagset!action2index(_Action,_Label,_LI,_Base,_I),
%	   	  format('registered action ~w\n',[_A])
%	        )),
%	  format('maxaction is actions=~w labels=~w\n',[MaxActions,NLabels]),
	  record(avgweight(700,0,0)),
	  record( parsed_options)
	),
	%%	format('adding persistent\n',[]),
	persistent!add(parsed_options),
	%% the following avoid reparsing options at each loop iteration
	%% using persistent facts
	persistent!add_fact(opt(_)),
	persistent!add_fact(beam(_)),
	persistent!add_fact(sbeam(_)),
	persistent!add_fact(tagset!label(_)),
	persistent!add_fact(tagset!label2index(_,_)),
	persistent!add_fact(tagset!nlabels(_)),
	persistent!add_fact(tagset!action2index(_,_,_,_,_)),
	persistent!add_fact(tagset!index2action(_,_,_)),
	persistent!add_fact(tagset!simple_label(_,_)),
	persistent!add_fact(tagset!block(_,_,_)),
	persistent!add_fact(templates(_,_)),
	persistent!add_fact(guide!xaction(_,_,_)),
	persistent!add_fact(tagset!pop0),
	persistent!add_fact(tagset!pop1),
	persistent!add_fact(tagset!swap),
	persistent!add_fact(tagset!noop),
	persistent!add_fact(tagset!swap2),
	persistent!add_fact(tagset!attach),
	persistent!add_fact(tagset!reduce),
	persistent!add_fact(tagset!reduce2),
	persistent!add_fact(tagset!ensure_tree),
	persistent!add_fact(tagset!late_attach),
	persistent!add_fact(tagset!allow_cycle),
	persistent!add_fact(tagset!actionbase2index(_,_)),
	persistent!add_fact(maxstep_factor(_)),
%	persistent!add_fact(avgweight(_,_,_)),
	true
	.

:-std_prolog read_dbfile/1.
read_dbfile(File) :-
	open(File,read,S),
	repeat(( read_term(S,T,_),
		 ( T == eof
		 xor T=tagset!label(Label),
		   update_counter(labels,I),
		   J is I+1,
		   record(tagset!label2index(Label,J)),
		   fail
		 xor
		   record(T),
		   fail
		 )
	       )),
	close(S)
	.

:-std_prolog item!tree/2.

item!tree(ItemAddr,ExitAddr) :-
    ( ItemAddr = 0 ->
%	  format('reaching void addr\n',[]),
	  true
      ; recorded(item{ step => 0 }, ItemAddr) ->
	 true
     ;
     safe_recorded( Item::item{ step => S, stack => Stack0::tree(Addr,_,_,_,_), stack1 => Stack1 }, ItemAddr),
     ( item!back(ItemAddr,K::shift(_,_,_),ExitAddr,ItemAddr1,W) ->
	   ItemAddr0 = ExitAddr,
	   Action = K,
	   true
       ; item!acceptable_back(ItemAddr,Action,ItemAddr0,ItemAddr1,W,ExitAddr),
	 \+ ( item!acceptable_back(ItemAddr,_Action,_ItemAddr0,_ItemAddr1,_W,ExitAddr),
	      _W > W
	    ) ->
	     true
       ; ExitAddr = 0 ->
%	     format('** pb reaching void addr\n',[]),
	     fail
       ;
       safe_recorded(item{ step => SExit },ExitAddr),
       format('*** pb item!tree step=~w item=~w exit=~w exit_step=~w\n',[S,ItemAddr,ExitAddr,SExit]),
       every(( _B::item!back(ItemAddr,_,_,_,_),
	       format('\tback ~w\n',[_B])
	    )),
       fail
     ),
     verbose('back process step=~w exit=~w addr=~w action=~w addr0=~w addr1=~w\n',[S,ExitAddr,ItemAddr,Action,ItemAddr0,ItemAddr1]),
     ( Action = left(Label) ->
	   safe_recorded( item{ stack => tree(Addr00,_,_,_,_) }, ItemAddr0),
	   record_without_doublon(result!dep(Addr00,Addr,Label)),
	   item!tree(ItemAddr1,ExitAddr),
	   item!tree(ItemAddr0,ItemAddr1)
       ; Action = right(Label) ->
	     safe_recorded( item{ stack1 => tree(Addr01,_,_,_,_) }, ItemAddr0),
	     record_without_doublon(result!dep(Addr01,Addr,Label)),
	     item!tree(ItemAddr1,ExitAddr),
	     item!tree(ItemAddr0,ItemAddr1)
       ; Action = lefta(Label) ->
	     Item = item{ stack1 => tree(Addr1,_,_,_,_) },
	     record_without_doublon(result!dep(Addr,Addr1,Label)),
	     item!tree(ItemAddr0,ExitAddr)
       ; Action = righta(Label) ->
	     Item = item{ stack1 => tree(Addr1,_,_,_,_) },
	     record_without_doublon(result!dep(Addr1,Addr,Label)),
	     item!tree(ItemAddr0,ExitAddr)
       ; Action = left2(Label) ->
	     Item = item{ stack1 => tree(Gov,_,_,_,_) },
	     safe_recorded( item{ stack => tree(Dep,_,_,_,_) }, ItemAddr0),
	     record_without_doublon(result!dep(Dep,Gov,Label)),
	     item!tree(ItemAddr1,ExitAddr),
	     item!tree(ItemAddr0,ItemAddr1)
       ; Action = right2(Label) ->
	     Item = item{ stack => tree(Gov,_,_,_,_) },
	     safe_recorded( item{ stack1 => tree(Dep,_,_,_,_) }, ItemAddr1),
	     record_without_doublon(result!dep(Dep,Gov,Label)),
	     item!tree(ItemAddr1,ExitAddr),
	     item!tree(ItemAddr0,ItemAddr1)
       ; Action=shift(_,_,_) ->
	     recorded( 'C'(Left,_,_,Right) , Addr ),
	     record_without_doublon(result!root(Left,Addr,Right)),
	     true
       ; Action=action[pop0,pop1] ->
	     item!tree(ItemAddr1,ExitAddr),
	     item!tree(ItemAddr0,ItemAddr1)
       ; Action=action[swap,noop,swap2] ->
	     item!tree(ItemAddr0,ExitAddr)
       ;
       %% should not be here !
       fail
    ),

     ((Action = lefta(_) xor Action = righta(_)) ->
	  recorded( 'C'(_,_,lemma{ cat => Cat0 },_), Addr ),
	  ( Stack1 = tree(Addr1,_,_,_,_), 
            \+ Addr1 = 0 ->
	       recorded( 'C'(_,_,lemma{ cat => Cat1 },_), Addr1 )
	   ; Cat1 = 0
	  ),
	  Info = [Cat0,Cat1]
      ;
      Info = []
     ),
     safe_recorded(item{ prefix => PW0}, ItemAddr0),
     Delta is W - PW0,
     format('%% step ~w action ~w info ~w cost=~w delta=~w\n',[S,Action,Info,W,Delta]),

     true
    )
.

:-extensional result!root/3.
:-extensional result!dep/3.
:-std_prolog tree!display/1.

tree!display(Left) :-
    result!root(Left,Addr,Right),
    recorded( 'C'(Left,Id,lemma{ lex => Lex, lemma => Lemma, fullcat => FCat, cat => Cat, mstag => FS },Right) , Addr ),
    XLeft is Left+1,
    (result!dep(Addr,_,_) xor record(result!dep(Addr,'',''))),
    every((info!contracted(XLeft,XRight,CForm),
	   format('~w-~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,XRight,CForm,'_','_','_','_','_','_','_'] )
	  )),
    every((info!ellipse(XLeft,Beta,CForm),
	   format('~w.~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,Beta,CForm,'_','_','_','_','_','_','_'] )
	  )),
    every(( result!dep(Addr,GovAddr,Label),
%	    format('** process dep ~w ~w ~w\n',[Addr,GovAddr,Label]),
	    ( GovAddr = 0 ->
		  Gov = 0
	      ; GovAddr = '' ->
		    Gov = ''
	      ;
	      recorded( 'C'(GLeft,_,_,_) , GovAddr ),
	      Gov is GLeft+1
	    ),
	    (opt(mstag) ->
		 (FS = [_|_] -> XFS = FS ; XFS = [FS]),
		 (Left=0,
		  recorded(sentence_id(SentID)) ->
		      XFS2=[SentID|XFS]
		  ;
		  XFS2 = XFS
		 ),
		 format('~w\t~w\t~w\t~w\t~w\t~L\t~w\t~w\t~w\n', [XLeft,Lex,Lemma,Cat,FCat,['~w','|'],XFS2,Gov,Label,Id] )
	     ;
	     format('~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,Lex,Lemma,Cat,FCat,'_',Gov,Label,Id] )
	    )
	 )),
    tree!display(Right)
.

#!/usr/bin/env perl

## build result files on SPMRL test files

use strict;
use AppConfig qw/:argcount :expand/;
use YAML::Any qw{LoadFile};

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "lang=s@" => {DEFAULT => []},
                            "mode=s@" => {DEFAULT => []},
                            "beam=d@" => {DEFAULT => []},
			    'cdir=f' => {DEFAULT => "/scratch/clerger/SPMRL13"},
			    'tdir=f' => {DEFAULT => "/scratch/clerger/SHARED_TASK_TEST_SETS"},
			    'mdir=f' => {DEFAULT => "/scratch/clerger"},
			    'run!' => {DEFAULT => 0},
			    'start=d' => {DEFAULT => '042'},
			    'best=f' => {DEFAULT => 'spmrl_best.yml'},
			    'team=s' => {DEFAULT => 'ALPAGE_DYALOG' },
			    'full!' => {DEFAULT => 1},
			    'malt_home=s' => {DEFAULT => "/home/rioa/clerger/ybuild64/src/maltparser-1.7.1"},
			    'malt_jar=s' => {DEFAULT => "maltparser-1.7.1.jar"},
			    'lattice=s',	# null | disamb | nodisamb
			    );

$config->args();

my @allmodes = ('gold', 'pred');

my $best = LoadFile($config->best);

my @alllangs = sort keys %$best;
my %proj = ( 'german' => 1,
	     'hungarian' => 1,
	     'swedish' => 1
	     );

my $run = $config->run;

my $mdir = $config->mdir;
my $cdir = $config->cdir;
my $tdir = $config->tdir;

my @beams = @{$config->beam};
@beams or @beams = qw{8 6 4};

my @modes = @{$config->mode};
## @modes or push(@modes,'gold');
@modes or @modes = @allmodes;

my @langs = @{$config->lang};
## @langs or push(@langs,'french');
@langs or @langs = @alllangs;

my $team = $config->team;
my $lattice = $config->lattice;

my $full = $config->full;
my $malt_home = $config->malt_home;
my $malt_jar = $config->malt_jar;

my @nodes = ( "042" .. "064", 
	      ## 65 seems to be dead
	      "066" .. "071",
	      ## 72 seems to be dead
	      "073" .. "078"
	      );

my @save = @nodes;

my $current = $config->start;
$current or $current ||= $nodes[0];

until (grep {$current eq $_} @nodes) {
    $current++;
}

until ($current eq $nodes[0]) {
    shift @nodes;
}

foreach my $beam (@beams) {
    my $lrun = beam2run($beam);
    foreach my $lang (@langs) {
	my $LANG = uc($lang);
	my $Lang = ($lang eq 'swedish') ? $lang :  ucfirst($lang);
	foreach my $mode (@modes) {
	    my $dir = "$mdir/spmrl_${lang}_${mode}.beam${beam}";
	    my $iter = $best->{$lang}{$beam}{$mode}{maxiter};
	    my $model = "$dir/ftb6.model.b${beam}i${iter}";
	    my $resdir = "$mdir/$team/${LANG}_SPMRL/test";
	    $run and system("mkdir -p $resdir");
	    my $size = 'full';
	    -d "$cdir/${LANG}_SPMRL/gold/conll/train" or $size = '5k';
	    $full or $size = '5k';
	    my $lfile = "test.${Lang}.${mode}.conll.tobeparsed.tagged";
	    my $testfile = "${tdir}/${LANG}_SPMRL/${mode}/conll/test/$lfile";
	    my $resfile = "${resdir}/${lfile}.parsed.${size}.${lrun}";
	    print "Process spmrl test lang=$lang mode=$mode run=$lrun : beam=$beam model=$model\n";
	    -f $model or die "missing model file $model";
	    print "\ttest file $testfile\n";
	    -f $testfile or die "missing test file $testfile";
	    print "\tsaving into $resfile\n";

	    my $tagset = "spmrl_${lang}";
	    $proj{$lang} and $tagset .= "_proj";
	    if ($lattice && $lattice eq 'disamb') {
		$tagset .= "_lattice";
	    } elsif ($mode eq 'pred')  {
		$tagset .= "_${mode}";
	    }
	    $tagset .= ".tagset.db";
	    @nodes or @nodes = @save;
	    my $node = shift @nodes;
	    $node = "node$node";
	    print "\trunning on node '$node'\n";
	    my $lexer_mode = '';
	    $mode eq 'pred' and $lexer_mode = '--mode pred';
#	    my $cmd = "pexec -n=$node -b -d=/home/rioa/clerger/ybuild64/src/dyalog-sr -- '( cat $testfile | yarecode -l=fr -u | ./conll2db.pl --tagset $tagset $lexer_mode | ./dpsr_parser -loop -utime -- -beam $beam -res $tagset -load_model $model -multi | ./answer2conll.pl | yadecode -l=fr -u > $resfile )'";
	    my $proj = "";
	    (exists $proj{$lang}) and $proj = " -proj pproj.$lang";
	    my $cmd = "pexec -n=$node -b -d=/home/rioa/clerger/ybuild64/src/dyalog-sr -- ./test.pl --beam $beam --model $model --res $resfile --tagset $tagset --mode $mode --test $testfile --resdir $resdir $proj  --malt_home ${malt_home} --malt_jar ${malt_jar}";
	    $run or print "\trunning cmd <$cmd>\n";
	    $run and system($cmd);
	}
    }
}

sub beam2run {
    my $n = shift;
    return 'run'.(5-int($n/2));
}

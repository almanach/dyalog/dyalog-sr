#!/usr/bin/env perl

## run a set of SPMRL experiments

use strict;
use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "lang=s@" => {DEFAULT => []},
                            "mode=s@" => {DEFAULT => []},
                            "beam=d@" => {DEFAULT => []},
                            "iter=d" => {DEFAULT => 10},
#			    'evalcmd=f' => {DEFAULT => "/home/rioa/clerger/ybuild64/src/corpus_proc/conll_eval.pl"},
			    'evalcmd=f' => {DEFAULT => "/home/rioa/clerger/ybuild64/src/dyalog-sr/eval07.pl"},
			    'cdir=f' => {DEFAULT => "/scratch/clerger/SPMRL13"},
			    'mdir=f' => {DEFAULT => "/scratch/clerger/"},
			    'run!' => {DEFAULT => 0},
			    'start=d' => {DEFAULT => 42},
			    'iter2=d',
			    'lattice=s',	# null | disamb | nodisamb
			    'full' => {DEFAULT => 1},
			    'malt_home=s' => {DEFAULT => "/home/rioa/clerger/ybuild64/src/maltparser-1.7.1"},
			    'malt_jar=s' => {DEFAULT => "maltparser-1.7.1.jar"}
			   );

$config->args();

my @allmodes = ('gold', 'pred');
##my @alllangs = ('french', 'german','swedish','hungarian','polish','basque','korean','hebrew','arabic');
my @alllangs = qw{french german swedish hungarian polish basque hebrew korean arabic};

my %proj = ( 'german' => 1,
	     'hungarian' => 1,
	     'swedish' => 1
	     );

my $run = $config->run;

my $mdir = $config->mdir;
my $evalcmd = $config->evalcmd;
my $cdir = $config->cdir;
my $iter = $config->iter;
my $iter2 = $config->iter2; # to be used for easy languages
my $full = $config->full;
my $malt_home = $config->malt_home;
my $malt_jar = $config->malt_jar;

$iter2 or $iter2 = $iter;

my @beams = @{$config->beam};
@beams or push(@beams,1);

my @modes = @{$config->mode};
@modes or push(@modes,'gold');
grep {$_ eq 'all'} @modes and @modes = @allmodes;

my @langs = @{$config->lang};
@langs or push(@langs,'french');
grep {$_ eq 'all'} @langs and @langs = @alllangs;

my $lattice = $config->lattice;

my @nodes = ( "042" .. "064", 
	      ## 65 seems to be dead
	      "066" .. "071",
	      ## 72 seems to be dead
	      "073" .. "078"
	      );

my @save = @nodes;

my $current = $config->start;
$current or $current ||= $nodes[0];

until (grep {$current eq $_} @nodes) {
    $current++;
}

until ($current eq $nodes[0]) {
    shift @nodes;
}

foreach my $beam (@beams) {
    foreach my $lang (@langs) {
	foreach my $mode (@modes) {
	    print "Process lang=$lang mode=$mode beam=$beam\n";
	    unless (@nodes) {
#		print "\t*** no more nodes available: skipping\n";
		@nodes = @save;
#		next;
	    }
	    my $yy = $full ? "" : "5k/";
	    my $xx = $lattice ? "_$lattice" : "";
	    my $dir = "$mdir/${yy}spmrl_${lang}_${mode}${xx}.beam${beam}";
	    if (-d $dir && -f "$dir/train.log") {
		print "cleaning existing result dir\n";
		open(LOG,"<","$dir/train.log") || die "can't open log file: $!";
		my $iter=0;
		while(<LOG>) {
		    /:\s+iteration\s+(\d+)$/ and $iter=$1;
		    /:\s+done/ and $iter=0;
		}
		if ($iter) {
		    # clean last iteration
		    foreach my $file (glob("$dir/*.b*i$iter")) {
			print "\tremove $file\n";
			$run and unlink $file;
		    }
		}
		close(LOG);
	    }
	    my $node = shift @nodes;
	    $node = "node$node";
	    my $liter = $iter;
	    (grep {$lang eq $_} qw{swedish polish basque hebrew hungarian}) and $liter = $iter2;
	    print "\trunning on node '$node'\n";
	    my $spmrl_flag = "${lang}_${mode}";
	    $full or $spmrl_flag = "${lang}5k_${mode}";
	    if ($lattice) {
		$spmrl_flag .= "_lattice.$lattice";
	    }
	    my $proj = "";
	    (exists $proj{$lang}) and $proj = " -proj";
	    my $cmd = "pexec -n=$node -d=/home/rioa/clerger/ybuild64/src/dyalog-sr -b -- ./train.pl --iter=$liter --cdir=$cdir --evalcmd=$evalcmd --spmrl=${spmrl_flag} --mdir=$dir --beam=$beam $proj --malt_home ${malt_home} --malt_jar ${malt_jar}";
	    $run or print "\trunning cmd <$cmd>\n";
	    $run and system($cmd);
	}
    }
}

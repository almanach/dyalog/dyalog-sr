#!/usr/bin/env perl

## train dpsr_parser in FTB6 on pred data

use strict;
use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "lang=s@" => {DEFAULT => []},
                            "mode=s@" => {DEFAULT => []},
                            "beam=d@" => {DEFAULT => []},
                            "iter=d" => {DEFAULT => 10},
			    'evalcmd=f' => {DEFAULT => "/home/rioa/clerger/ybuild64/src/corpus_proc/conll_eval.pl"},
#			    'evalcmd=f' => {DEFAULT => "/home/rioa/clerger/ybuild64/src/dyalog-sr/eval07.pl"},
			    'cdir=f' => {DEFAULT => "/projdata/alpage/parserevision/data"},
			    'mdir=f' => {DEFAULT => "/scratch/clerger/"},
			    'run!' => {DEFAULT => 0},
			    'start=d' => {DEFAULT => 42}
			   );

$config->args();

my $run = $config->run;

my $mdir = $config->mdir;
my $evalcmd = $config->evalcmd;
my $cdir = $config->cdir;
my $iter = $config->iter;

my @beams = @{$config->beam};
@beams or push(@beams,1);

my @nodes = ( "042" .. "064", 
	      ## 65 seems to be dead
	      "066" .. "071",
	      ## 72 seems to be dead
	      "073" .. "078"
	      );

my @save = @nodes;

my $current = $config->start;
$current or $current ||= $nodes[0];

until (grep {$current eq $_} @nodes) {
    $current++;
}

until ($current eq $nodes[0]) {
    shift @nodes;
}

foreach my $beam (@beams) {
    print "Process FTB6 beam=$beam\n";
    unless (@nodes) {
#		print "\t*** no more nodes available: skipping\n";
	@nodes = @save;
#		next;
    }
    my $dir = "$mdir/ftb6_pred.beam${beam}";
    if (-d $dir && -f "$dir/train.log") {
	print "cleaning existing result dir\n";
	open(LOG,"<","$dir/train.log") || die "can't open log file: $!";
	my $iter=0;
	while(<LOG>) {
	    /:\s+iteration\s+(\d+)$/ and $iter=$1;
	    /:\s+done/ and $iter=0;
	}
	if ($iter) {
	    # clean last iteration
	    foreach my $file (glob("$dir/*.b*i$iter")) {
		print "\tremove $file\n";
		$run and unlink $file;
	    }
	}
	close(LOG);
    }
    my $node = shift @nodes;
    $node = "node$node";
    print "\trunning on node '$node'\n";
    my $cmd = "pexec -n=$node -d=/home/rioa/clerger/ybuild64/src/dyalog-sr -b -- ./train.pl --iter=$iter --cdir=$cdir --train ftb.1.pred-ltm.conll --dev ftb.2.pred-ltm.conll --test ftb.3.pred-ltm.conll --evalcmd=$evalcmd --mdir=$dir --beam=$beam --tagset ftb6.tagset.conll";
    $run or print "\trunning cmd <$cmd>\n";
    $run and system($cmd);
}


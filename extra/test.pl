#!/usr/bin/env perl

## script for running elementary test

use strict;
use POSIX qw(strftime);
use AppConfig;

my $config = AppConfig->new(
                            'verbose|v!' => {DEFAULT => 0},
			    'test=f',
			    'res=f',
			    'resdir=f',
			    'beam=d' => {DEFAULT => 1},
			    'model=s' => {DEFAULT => 'ftb6.model'},
			    'tagset=s' => 
			    'mode=s' => {DEFAULT => 'gold'}, 
			    'malt_home=s' => {DEFAULT => $ENV{MALT_HOME}},
			    'malt_jar=s' => {DEFAULT => 'maltparser-1.7.1.jar'},
			    'proj=s' => {DEFAULT => 0} # projectivize
			   );

$config->args;

my $model=$config->model;
my $test = $config->test;
my $beam = $config->beam;
my $tagset = $config->tagset;
my $mode = $config->mode;
my $res = $config->res;
my $resdir = $config->resdir;

my $proj = $config->proj;
my $malt_home = $config->malt_home;
my $malt_jar = $config->malt_jar;

-f $tagset
  or die "missing tagset file '$tagset': $!";

-d $resdir
    or system("mkdir -p $resdir");

my $hostname = `hostname`;
chomp $hostname;

my $lexer_mode = '';
($mode eq 'pred') and $lexer_mode = "--mode pred";

print "$hostname: running test=$test res=$res tagset=$tagset beam=$beam mode=$mode\n";

my $dounproj = "";
if ($proj) {
#  $dounproj = "| java -jar ${malt_home}/${malt_jar} -c $proj -m deproj -i /dev/stdin -o /dev/stdout";
}

system("cat $test | yarecode -l=fr -u |./conll2db.pl --tagset $tagset $lexer_mode | ./dpsr_parser -loop -utime -- -beam $beam -res $tagset -load_model $model -multi | ./answer2conll.pl | yadecode -l=fr -u $dounproj | ./spmrl_align.pl $test > $res") == 0
   or die "system failed: $?" ;



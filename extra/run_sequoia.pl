#!/usr/bin/env perl

## parse sequoia corpus with dpsr parser

use strict;
use AppConfig qw/:argcount :expand/;
use YAML::Any qw{LoadFile};
use File::Basename;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "beam=d" => {DEFAULT => 8},
			    'cdir=f' => {DEFAULT => "/pinotdata/Corpus/sequoia-proj-conll.v3"},
			    'mdir=f' => {DEFAULT => "/scratch/clerger"},
			    'best=f' => {DEFAULT => 'ftb6_best.yml'},
			    'run!' => {DEFAULT => 0},
			    'start=d' => {DEFAULT => '042'}
			    );

$config->args();

my $best = LoadFile($config->best);

my $run = $config->run;

my $mdir = $config->mdir;
my $cdir = $config->cdir;

-d $cdir or die "bad corpus directory $cdir";

my %corpus = map {basename($_,qw{.conll .p_conll}) => $_} glob("$cdir/*.p_conll");

my $beam = $config->beam;
		  
my @nodes = ( "042" .. "064", 
	      ## 65 seems to be dead
	      "066" .. "071",
	      ## 72 seems to be dead
	      "073" .. "078"
	      );

my @save = @nodes;

my $current = $config->start;
$current or $current ||= $nodes[0];

until (grep {$current eq $_} @nodes) {
    $current++;
}

until ($current eq $nodes[0]) {
    shift @nodes;
}

my $iter = $best->{$beam}{maxiter};
my $dir = "$mdir/ftb6.beam${beam}";
my $model = "$dir/ftb6.model.b${beam}i${iter}";

-f $model or die "missing model file $model";

my $resdir = "$mdir/sequoia.beam${beam}";
$run and system("mkdir -p $resdir");

foreach my $base (keys %corpus) {
    print "processing $base gold=$corpus{$base}\n";
    my $testfile = $corpus{$base};
    my $resfile = "${resdir}/$base.conll";
    -f $testfile or die "missing test file $testfile";
    my $tagset = "ftb6.tagset.conll";
    @nodes or @nodes = @save;
    my $node = shift @nodes;
    $node = "node$node";
    print "\trunning on node '$node'\n";
    my $lexer_mode = '';
    my $cmd = "pexec -n=$node -b -d=/home/rioa/clerger/ybuild64/src/dyalog-sr -- ./test.pl --beam $beam --model $model --res $resfile --tagset $tagset --test $testfile --resdir $resdir";
    $run or print "\trunning cmd <$cmd>\n";
    $run and system($cmd);
}


#!/usr/bin/env perl

## training script

use strict;
use POSIX qw(strftime);
use AppConfig;

my $config = AppConfig->new(
                            'verbose|v!' => {DEFAULT => 0},
			    'train=f' => {DEFAULT => "pcedt.train.planar.empty.sagae"},
			    'dev=f' => {DEFAULT => "pcedt.sec20.dev.sagae"},
                            'cdir=f' => { DEFAULT => "/Users/clergeri/Work/Corpus/SemEval14/Data" },
                            'iter=d' => {DEFAULT => 10},
			    'beam=d' => {DEFAULT => 1},
			    'model=s' => {DEFAULT => 'semeval.model'},
			    'evalcmd=f' => {DEFAULT => "/Users/clergeri/Work/Corpus/SemEval14/eval-toolkit/dist/sdp.jar"},
			    'log=f' => { DEFAULT => "train.log" },
			    'mdir=s' => {DEFAULT => 'semeval'},
			    'tagset=s' => {DEFAULT => 'semeval_pcedt_planar_empty.tagset'},
			    'mode=s' => {DEFAULT => 'early'}, # early, late, best
			    'test=s',
			    'sbeam=d' => {DEFAULT => 3},
			    'random_skip=d' => {DEFAULT => 0}
			   );

$config->args;

my $model_base=$config->model;
my $cdir = $config->cdir;
my $evalcmd = $config->evalcmd;
my $train = $config->train;
my $dev = $config->dev;
my $test = $config->test;
my $beam = $config->beam;
my $maxiter = $config->iter;
my $tagset = $config->tagset;
my $mode = $config->mode;
my $skip = $config->random_skip;

my $sbeam = "-sbeam ".$config->sbeam;

-f $tagset
  or die "missing tagset file '$tagset': $!";

my $mdir = $config->mdir;

-d $mdir 
  or mkdir $mdir;

-d $mdir
  or die "couldn't build model directory '$mdir': $!";

my $hostname = `hostname`;
chomp $hostname;

my $logfile = $config->log;
open(LOG,">$mdir/$logfile") || die "can't open logfile '$mdir/$logfile': $!";

verbose("training hostname=$hostname iter=$maxiter train=$train dev=$dev mode=$mode beam=$beam model=$model_base mdir=$mdir");

my $gtrain = "$cdir/train/$train";
my $gdev = "$cdir/dev/$dev";
my $gtest;

my $sdpgdev = $gdev;
$sdpgdev =~ s/\.dev.*\.(sagae|conll)$/.dev.sdp/o;

$test and $gtest = "$cdir/test/$test";

foreach my $iter (1..$maxiter) {
  verbose("iteration $iter");
  my $prev = $iter-1;
  my $suff = "b${beam}i${iter}";
  my $model = "$mdir/$model_base.$suff";
  my $ldev = "$mdir/$dev.$suff";
  my $sdpldev = $ldev;
  $sdpldev =~ s/\.dev.*\.(sagae|conll)/.dev.sdp/o;
  my $aldev = "$mdir/$dev.$suff";
  $aldev =~ s/\.dev.*\.(sagae|conll)/.dev.answer/o;
  my $eval = "$mdir/$dev.eval.$suff";
  my $in = $prev ? "-load_model $mdir/$model_base.b${beam}i${prev}" : "";
  my $out = "-save_model $model";
  my $lexer_opts = "--tagset $tagset --graph";
  my $train_lexer_opts = $lexer_opts;
  $train_lexer_opts .= " -oracle ";
  $skip and $train_lexer_opts .= " --random_skip=$skip";
  verbose("\tbuilding $model on $train");
  my $cmd = "cat $gtrain | yarecode -l=fr -u | ./conll2db.alt.pl $train_lexer_opts 2> $mdir/lexer_errs.log | tee $mdir/lexer.log |./dpsr_parser -loop -utime -- -train $mode -beam $beam $sbeam -res $tagset $in $out -multi -mstag 2> $mdir/parser_err.log > $mdir/$train.answer.$suff";
#  print STDERR "$cmd\n";
#  unless (-f $model) {
  unless (-f "$mdir/$train.answer.$suff") {
    system("$cmd") == 0
      or die "system failed: $?";
  }
  if (1) {
    verbose("\trunning on dev $dev");
    unless (-f $ldev) {
      system("cat $gdev| yarecode -l=fr -u |./conll2db.alt.pl $lexer_opts | ./dpsr_parser -loop -utime -- -beam $beam $sbeam -res $tagset -load_model $model -multi -mstag | tee $aldev | ./answer2conll.pl| cut -f1-8 | yadecode -l=fr -u | tee $ldev | ./sagae2sdp.pl > $sdpldev") == 0
	or die "system failed: $?" ;
    }
  }
  if (1){
    my $tmp = "java -cp $evalcmd sdp.tools.Evaluator $sdpgdev $sdpldev";
    verbose("run eval: $tmp\n");
    system("$tmp 2> $eval") == 0
      or die "system failed: $?";
    my $data = process_eval_file($eval);
    verbose("\tmodel=$model LP=$data->{LP} LR=$data->{LR} LF=$data->{LF} UP=$data->{UP} UR=$data->{UR} UF=$data->{UF}");
    close(EVAL);
  }
}

verbose("done");

sub verbose {
    my $x = shift;
    my $date = strftime "[%F %H:%M:%S]", localtime;
    my $msg = "$date : $x\n";
    print LOG $msg;
    print $msg;
}

sub process_eval_file {
  my $eval = shift;
  (-f $eval)
    or die "missing eval file $eval";
  open(EVAL,"<",$eval)
    || die "can't open eval file $eval: $!";
  my $data = {};
  while(my $line = <EVAL>) {
    $line =~ /^## Scores excluding/ and last;
    $line =~ /^([LU][PRF]):\s+(\S+)$/ or next;
    my $type = $1;
    my $score = $2;
    $score =~ s/,/./o;
    $data->{$type} = sprintf("%.2f",100 * $score);
  }
  close(EVAL);
  return $data;
}

close(LOG);

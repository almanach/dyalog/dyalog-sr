#!/usr/bin/env perl

## training script

use strict;
use POSIX qw(strftime);
use AppConfig;

my $config = AppConfig->new(
                            'verbose|v!' => {DEFAULT => 0},
			    'train=f' => {DEFAULT => "ftb6_1.conll"},
			    'dev=f' => {DEFAULT => "ftb6_2.conll"},
                            'cdir=f' => { DEFAULT => "/Users/clergeri/Work/Corpus/FTB6/" },
                            'iter=d' => {DEFAULT => 10},
			    'beam=d' => {DEFAULT => 1},
			    'model=s' => {DEFAULT => 'ftb6.model'},
			    'evalcmd=f' => {DEFAULT => "/Users/clergeri/Work/corpus_proc/conll_eval.alt.pl"},
			    'log=f' => { DEFAULT => "train.log" },
			    'mdir=s' => {DEFAULT => 'models'},
			    'tagset=s' => {DEFAULT => 'ftb6_lattice.tagset.conll'},
			    'mode=s' => {DEFAULT => 'early'}, # early, late, best
			    'lattice=s',
			    'test=s',
			   );

$config->args;

my $model_base=$config->model;
my $cdir = $config->cdir;
my $evalcmd = $config->evalcmd;
my $train = $config->train;
my $dev = $config->dev;
my $test = $config->test;
my $beam = $config->beam;
my $maxiter = $config->iter;
my $tagset = $config->tagset;
my $mode = $config->mode;

my $lattice_flag = $config->lattice;

-f $tagset
  or die "missing tagset file '$tagset': $!";

my $mdir = $config->mdir;

-d $mdir 
  or mkdir $mdir;

-d $mdir
  or die "couldn't build model directory '$mdir': $!";

my $hostname = `hostname`;
chomp $hostname;

my $logfile = $config->log;
open(LOG,">$mdir/$logfile") || die "can't open logfile '$mdir/$logfile': $!";

verbose("training hostname=$hostname iter=$maxiter train=$train dev=$dev mode=$mode beam=$beam model=$model_base mdir=$mdir");

my $gtrain = "$cdir/$train";
my $gdev =  "$cdir/$dev";
my $gtest;

$test and $gtest = "$cdir/$test";

foreach my $iter (1..$maxiter) {
  verbose("iteration $iter");
  my $prev = $iter-1;
  my $suff = "b${beam}i${iter}";
  my $model = "$mdir/$model_base.$suff";
  my $ldev = "$mdir/$dev.$suff";
  my $eval = "$mdir/$dev.eval.$suff";
  my $in = $prev ? "-load_model $mdir/$model_base.b${beam}i${prev}" : "";
  my $out = "-save_model $model";
  my $lexer_opts = "--tagset $tagset";
  my $train_lexer_opts = $lexer_opts;
  $train_lexer_opts .= " -oracle ";
  my $goldseg = $gtrain;
  $goldseg =~ s/\.lattices$/.l1.conll/o;
  $train_lexer_opts .= " --oracle_seg=$goldseg --lattice";
  print "LATTICE MODE train_lexer_opts=$train_lexer_opts\n";
  verbose("\tbuilding $model on $train");
  my $cmd = "cat $gtrain | yarecode -l=fr -u | ./conll2db.alt.pl $train_lexer_opts 2> $mdir/lexer_errs.log | tee $mdir/lexer.log |./dpsr_parser -loop -utime -- -train $mode -beam $beam -res $tagset $in $out -multi 2> $mdir/parser_err.log > $mdir/$train.answer.$suff";
#  print STDERR "$cmd\n";
  (-f "$model") or
    system("$cmd") == 0
      or die "system failed: $?";
  verbose("\trunning on dev $dev");
  $lexer_opts .= " --lattice ";
  (-f $ldev) or
    my $conllgdev = $gdev;
    $conllgdev =~ s/\.lattices$/.l1.conll/o;
    system("cat $gdev| yarecode -l=fr -u |./conll2db.alt.pl $lexer_opts | ./dpsr_parser -loop -utime -- -beam $beam -res $tagset -load_model $model -multi | ./answer2conll.pl |  yadecode -l=fr -u > $ldev") == 0
       or die "system failed: $?" ;
  if (1) {
    system("$evalcmd -g $conllgdev -s $ldev 2> /dev/null > $eval") == 0
      or die "system failed: $?";
    my $stat = `grep "Labeled   attachment score" $eval`;
    chomp $stat;
    $stat =~ /(\S+)\s+\%/ and verbose("\tmodel=$model LAS=$1");
  }
  if ($gtest) {
    my $ltest = "$mdir/$test.$suff";
    my $conllgtest = $gtest;
    $conllgtest =~ s/\.lattices$/.l1.conll/o;
    (-f $ltest) or
      system("cat $gtest| yarecode -l=fr -u |./conll2db.alt.pl $lexer_opts | ./dpsr_parser -loop -utime -- -beam $beam -res $tagset -load_model $model -multi | ./answer2conll.pl |  yadecode -l=fr -u > $ltest") == 0
	or die "system failed: $?" ;
    system("$evalcmd -g $conllgtest -s $ltest 2> /dev/null > $eval") == 0
      or die "system failed: $?";
    my $stat = `grep "Labeled   attachment score" $eval`;
    chomp $stat;
    $stat =~ /(\S+)\s+\%/ and verbose("\tmodel=$model LAStest=$1");
  }
}

verbose("done");

sub verbose {
    my $x = shift;
    my $date = strftime "[%F %H:%M:%S]", localtime;
    my $msg = "$date : $x\n";
    print LOG $msg;
    print $msg;
}

close(LOG);

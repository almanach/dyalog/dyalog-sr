#!/usr/bin/env perl

# a simple converter from Sagae format to SDP format, to avoid use of python2.7

use strict;

my $graph={};
my $sid;
my $govs={};

while(<>) {
  if (/^\s*$/) {
    # end of sentence
    my $map = {};
    my $i = 0;
    foreach my $gov (sort {$a <=> $b} keys %$govs) {
      $map->{$gov} = $i++;
    }
    my $ngovs = scalar(keys %$govs);
    print "#$sid\n";
    foreach my $id (sort {$a <=> $b} keys %$graph) {
      my $data = $graph->{$id};
      my $isgov = exists $govs->{$id} ? '+' : '-';
      my @out = ($id,$data->[0][1],$data->[0][2],$data->[0][3],'-',$isgov,('_') x $ngovs);
      foreach my $entry (@$data) {
	my $lgov = $entry->[6];
	($lgov eq '0') and $out[4] = '+';
	$lgov and $out[6+$map->{$lgov}] = $entry->[7];
      }
      print join("\t",@out),"\n";
    }
    print "\n";
    # reset
    $graph={};
    $govs={};
    undef $sid;
    next;
  }
  chomp;
  my @f = split(/\t/,$_);
  ($f[5] =~ /sentid=(\d+)/) and $sid = $1;
  if (my $gov = $f[6]) {
    $govs->{$gov} = 1;
  }
  push(@{$graph->{$f[0]}},\@f);
}

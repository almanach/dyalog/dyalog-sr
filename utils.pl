/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  utils.pl -- 
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require('format.pl').
:-op(  800, xfx, [::=]).

:-extensional opt/1.

:-xcompiler
X ::= Y	:- '$interface'('DyALog_Copy'(Y:term,X:term),[]).

:-xcompiler
mutable_add(M,X) :- '$interface'('DyALog_Mutable_Add'(M:ptr,X:int),[return(none)]).	


:-xcompiler
verbose(Msg,Args) :-
    (   % uncomment the following line to remove all verbose tests
	%fail,
	  recorded( opt(verbose) ) ->
	      format(Msg,Args),
	      %	      flush_output,
	      true
	;
	    true
	)
.	

:-std_prolog show_time/1.

show_time(Step) :-
	( (Step==latency xor opt(multi)) ->
	  utime(Time),
	  ( recorded(utime(M)) ->
	    mutable_read(M,OldTime),
	    Delta is Time - OldTime,
	    fast_mutable(M,Time)
	  ;
	    mutable(M,Time),
	    record(utime(M)),
	    Delta = Time
	  ),
	  format('<~w_time> ~wms\n',[Step,Delta])
	;
	  true
	)
	.

:-xcompiler
fast_mutable(M,T) :-
	X ::= T,
	mutable(M,X)
	.


:-std_prolog utime/1.

utime(T) :- '$interface'('DyALog_Utime'(),[return(T:int)]).

:-xcompiler
abolish(F/N) :- '$interface'( 'Abolish'(F:term,N:int), [return(none)]).

:-xcompiler
perceptron!reset :- '$interface'('perceptron_reset',[return(none)]).

:-xcompiler
perceptron!advance :- '$interface'('perceptron_advance',[return(none)]).

:-xcompiler
delete_address(X) :- '$interface'( 'object_delete'(X:ptr),[return(bool)]).

:-std_prolog name_builder/3.

name_builder(Format,Args,Name) :-
        ( string_stream(_,S),
          format(S,Format,Args),
          flush_string_stream(S,Name),
          close(S)
        xor
           format('** pb name builder: ~w ~w ~w\n',[Format,Args,Name]),
           close(S),
           fail
        )
        .
:-std_prolog update_counter/2.

update_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
            mutable_inc(M,V)
        ;   V=0,
            mutable(M,1),
            record( counter(Name,M) )
        )
        .

:-std_prolog value_counter/2.

value_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
            mutable_read(M,V)
        ;
            V = 0
        )
        .

:-std_prolog child_extend/3.

child_extend(Label, Child, NewChild ) :-
	( Child = [] -> NewChild = [Label]
	; domain(Label,Child) -> NewChild = Child
	; child_add(Label,Child,NewChild)
	)
	.

:-std_prolog child_add/3.

child_add(Label,Child,NewChild) :-
	( Child = [] -> NewChild = [Label]
	; Child = [LabelA|Child2],
	  ( Label @< LabelA ->
	    NewChild = [Label|Child]
	  ; child_add(Label,Child2,NewChild2),
	    NewChild = [LabelA|NewChild2]
	  )
	)
	.

:-xcompiler
mutable_list_extend(M,X) :- '$interface'('DyALog_Mutable_List_Extend'(M:ptr,X:term),[return(none)]).


:-xcompiler
mutable_min(M,V) :-
	'$interface'('DyALog_Mutable_Min'(M:ptr,V:int),[return(none)])
	.

:-xcompiler
mutable_max(M,V) :-
	'$interface'('DyALog_Mutable_Max'(M:ptr,V:int),[return(none)])
	.


:-xcompiler
mutable_check_min(M,V) :-
	'$interface'('DyALog_Mutable_Check_Min'(M:ptr,V:int),[])
	.

:-xcompiler
mutable_check_max(M,V) :-
	'$interface'('DyALog_Mutable_Check_Max'(M:ptr,V:int),[])
	.

:-xcompiler
once(G) :- (G xor fail)
.

:-xcompiler
once_xor_true(G) :- (G xor true)
.


format_hook(0'L,Stream,[[Format,Sep],AA|R],R) :- %'0
        mutable(M,0,true),
        every((   domain(A,AA),
                  mutable_inc(M,V),
                  (   V == 0 xor write(Stream,Sep) ),
                  format(Stream,Format,[A]) ))
        .

:-xcompiler
safe_recorded(Item,ItemAddr) :-
%    verbose('try safe_recorded ~w\n',[ItemAddr]),
    recorded(Item,ItemAddr),
%    verbose('=> safe_recorded ~w:~w\n',[ItemAddr,Item]),
    true
	.

:-xcompiler
on_verbose(G) :-  (opt(verbose) -> G ; true).


:-xcompiler
persistent!add_fact(Fact) :-
	every(( recorded(Fact),
		persistent!add(Fact),
		true
	      ))
	.

:-xcompiler
record_without_doublon( A ) :-
        (recorded( A ) xor record( A ))
        .

%% to store a fact in DyALog persistent aread, to be restored at the next loop
%% NOTE: after restoration, the fact is removed from the persistent area

:-xcompiler
persistent!add(Fact) :-
	'$interface'('DyALog_Persistent_Add'(Fact:term),[return(none)]).


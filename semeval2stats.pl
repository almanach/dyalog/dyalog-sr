#!/usr/bin/env perl

## build HTML stat file from SemEval log

use strict;
use AppConfig qw/:argcount :expand/;

use Text::Table;
use CGI::Pretty qw/:standard *table *ul/;
use List::Util qw/min max/;
use Template;
use File::Slurp qw{slurp};
use YAML::XS qw{DumpFile};
use Date::Manip;

$YAML::XS::QuoteNumericStrings=0;

my $info = {
	    semeval => { lang => 'pcedt', 
			 train => 32389, 
			 dev => 1614, 
			 test => 0, 
			 devtokens => 36508
			 },
	    pcedt => { lang => 'pcedt', 
			 train => 32389, 
			 dev => 1614, 
			 test => 0, 
			 devtokens => 36508
			 },
	    pas => { lang => 'pas', 
			 train => 32389, 
			 dev => 1614, 
			 test => 0, 
			 devtokens => 36508
			 },
	    dm => { lang => 'dm', 
			 train => 32389, 
			 dev => 1614, 
			 test => 0, 
			 devtokens => 36508
			 },

	   };

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "html=f" => {DEFAULT => "semeval_stats.html"},
			    "yaml=f" => {DEFAULT => "semeval_best.yml"},
			    "csv=f",
			    "scp=f",
			    "loop!" => {DEFAULT => 0}
			    );

$config->args();

my @dir = @ARGV;

@dir or @dir = glob("*.beam*");

my $html = $config->html;
my $scp = $config->scp;
my $loop = $config->loop;

my $csv = $config->csv;

my $template = slurp(\*DATA);
 
my $hosts = {};
my $oldres = {};

process_all_old_res();

my $qr= qr/(\w+)\.beam(\d+)/;

while(1) {

    my %beam = ();
    my %lang=();
    my $niters = 0;
    my %best = ();

    foreach my $dir (@dir) {
	-d $dir or next;
	$dir =~ /$qr/o or next;
	my ($lang,$beam) = ($1,$2);
	my $mode ='gold';
	$beam{$beam}{$lang}{dir} = $dir;
	print "Here lang=$lang mode=$mode beam=$beam in $dir\n";
	my $log = "$dir/train.log";
	-f $log or next;
	open(LOG,"<",$log) || die "can read log file $log: $!";
	my $iter;
	my $prevdate;
	my $date;
	my $allspeed=0;
	my $nspeed=0;
	my $devtokens = $info->{$lang}{devtokens};
	while(<LOG>) {
	    /:\straining\s+hostname=(\S+)/ and $hosts->{$beam} = $1;
	    /:\s+iteration\s+(\d+)/ and $iter=$1;
	    if (/^\[(.+?)\]/) {
		$prevdate = $date;
		$date = $beam{$beam}{$lang}{$mode}{timestamp} = $1;
	    }
	    /\s+(?:LF|LAS)=(\S+)/ or next;
	    my $score = $1;
	    my $data = $beam{$beam}{$lang}{$mode}{iter}{$iter} ||= {};
	    while (/([LU][PRF])=(\S+)/g) {$data->{$1} = $2 }
            my $delta = UnixDate($date,"%s") - UnixDate($prevdate,"%s");
	    if ($delta && ($devtokens / $delta) < 1000 ) {
		$allspeed += $devtokens / $delta;
		$nspeed ++;
		$lang{$lang}{$beam}{$mode}{speed} = $allspeed / $nspeed;
	    }
	    $beam{$beam}{$lang}{$mode}{iter}{$iter}{LF} = $score;
            $lang{$lang}{$beam}{$mode}{iter}{$iter} = { %$data };
	    if ($beam{$beam}{$lang}{$mode}{max}{LF} < $score) {
		$beam{$beam}{$lang}{$mode}{max} = { %$data };
		$beam{$beam}{$lang}{$mode}{maxiter} = $iter;
	    }
	    if ($lang{$lang}{$beam}{$mode}{max}{LF} < $score) {
		$lang{$lang}{$beam}{$mode}{max} = { %$data };
		$lang{$lang}{$beam}{$mode}{maxiter} = $iter;
	    }
	    if ($best{$lang}{$mode}{max}{LF} < $score) {
		$best{$lang}{$mode}{max} = { %$data };
		$best{$lang}{$mode}{maxiter} = $iter;
		$best{$lang}{$mode}{beam} = $beam;
	    }
	    $niters < $iter and $niters = $iter;
	}
	close(LOG);
    }

    
    my $tt=Template->new({});
    
    $tt->process(\$template,
		 {
		     beams => \%beam,
		     langs => \%lang,
		     best => \%best,
		     iters => [1 .. $niters],
		     hosts => $hosts,
		     oldres => $oldres,
		     info => $info,
		 },
		 $html
		 )
	or die "can't process template: $!";

    $scp and system("scp $html $scp");

    DumpFile($config->yaml,\%lang);

    $loop or last;
    sleep(120);
}


sub process_old_res {
    my $dir = shift;
    -d $dir or next;
    my ($date) = $dir =~ /old(\S+)/;
    my @dir = glob("$dir/*.beam*");
    foreach my $ldir (@dir) {
	-d $ldir or next;
	$ldir =~ /(\w+)\.beam(\d+)/ or next;
	my $lang = $1;
	my $beam = $2;
	my $mode = 'gold';
	my $log = "$ldir/train.log";
	-f $log or next;
	open(LOG,"<",$log) || die "can read log file $log: $!";
	my $iter;
	while(<LOG>) {
	    /:\s+iteration\s+(\d+)/ and $iter=$1;
	    /\s+(?:LF|LAS)=(\S+)/ or next;
	    my $score = $1;
	    if ($score > $oldres->{$lang}{$mode}{max}) {
		$oldres->{$lang}{$mode} = { max => $score, dir => $date, beam => $beam, iter => $iter };
	      }
	  }
	close(LOG);
      }
  }

sub process_all_old_res {
    process_old_res($_) foreach (glob("old*"));
}

##    <meta http-equiv="REFRESH" content="120">

__END__
[%- USE date -%]
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SemEval charts (dpsr_parser)</title>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <style type="text/css">
     <!--
       .hilight { background: lightgreen; }
     -->
   </style>
   <script type="text/javascript" language="javascript">
<!--
        function showhide(obj) {
           var el=document.getElementById(obj);
           if (el.style.display == "none") {
              el.style.display = "block";
           } else {
              el.style.display = "none";
           }
        }
        function show(obj) {
           var el=document.getElementById(obj);
           el.style.display = "block";
        }
      $(function() {
         $( "#tabs" ).tabs();
         $( "#tabs-1" ).tabs();
         $( "#tabs-2" ).tabs();
         $( "#tabs-3" ).tabs();
      });
-->
  </script>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
    
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1', {'packages':['corechart','table']});
      
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawTables);
      
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawTables() {

      // Create the data tables.

      var data = new google.visualization.DataTable();

      var chart = new google.visualization.Table(document.getElementById('chart_best'));
      data.addColumn('string', 'lang');
      data.addColumn('number', 'LP (% dev)');
      data.addColumn('number', 'LR (% dev)');
      data.addColumn('number', 'LF (% dev)');
      data.addColumn('number', 'UP (% dev)');
      data.addColumn('number', 'UR (% dev)');
      data.addColumn('number', 'UF (% dev)');
      data.addColumn('number', 'best beam');
      data.addColumn('number', 'best iter');

      data.addColumn('number', '#sent (train)');
      data.addColumn('number', '#sent (dev)');

      data.addColumn('number', 'LF (%,old)');
      data.addColumn('string', 'dir (old)');
      data.addColumn('number', 'best beam (old)');
      data.addColumn('number', 'best iter (old)');

[% FOREACH l IN best.keys %]
     data.addRow([ '[% info.$l.lang %]', 
                    [% best.$l.gold.max.LP || 'null' %], 
                    [% best.$l.gold.max.LR || 'null' %], 
                    [% best.$l.gold.max.LF || 'null' %], 
                    [% best.$l.gold.max.UP || 'null' %], 
                    [% best.$l.gold.max.UR || 'null' %], 
                    [% best.$l.gold.max.UF || 'null' %], 
		    [% best.$l.gold.beam || 'null' %], 
		    [% best.$l.gold.maxiter || 'null' %], 

		    [% info.$l.train %],
		    [% info.$l.dev %],

                    [% oldres.$l.gold.max || 'null' %], 
		    '[% oldres.$l.gold.dir %]', 
		    [% oldres.$l.gold.beam || 'null' %], 
		    [% oldres.$l.gold.iter || 'null' %] 

		 ]);
[% END %]

    var formatter = new google.visualization.ColorFormat();
    formatter.addGradientRange(80,95,'white','#7BB77E','#37B73D');
    formatter.addGradientRange(70,80,'white','#F48404','#F2D9AE');
    formatter.format(data,1);
    formatter.format(data,2);
    formatter.format(data,3);
    formatter.format(data,4);
    formatter.format(data,5);
    formatter.format(data,6);
    formatter.format(data,11);

    var formatter = new google.visualization.NumberFormat({fractionDigits: 2});
    formatter.format(data,1);
    formatter.format(data,2);
    formatter.format(data,3);
    formatter.format(data,4);
    formatter.format(data,5);
    formatter.format(data,6);
    formatter.format(data,11);

    chart.draw(data, {width: '70em',  allowHtml: true});

[% FOREACH b IN beams.keys.nsort %]
      var data = new google.visualization.DataTable();

      var chart = new google.visualization.Table(document.getElementById('chart_beam_[%- b %]'));
      data.addColumn('string', 'lang');
      data.addColumn('number', 'LP (% dev)');      
      data.addColumn('number', 'LR (% dev)');
      data.addColumn('number', 'LF (% dev)');
      data.addColumn('number', 'UP (% dev)');      
      data.addColumn('number', 'UR (% dev)');
      data.addColumn('number', 'UF (% dev)');
      data.addColumn('number', 'best iter');
      data.addColumn('number', '#iters');

      data.addColumn('number', '#sent (train)');
      data.addColumn('number', '#sent (dev)');

      data.addColumn('string', 'host');
      data.addColumn('string', 'last entry');

[% FOREACH l IN beams.$b.keys %]
     data.addRow([ '[% info.$l.lang || l %]', 
                    [% beams.$b.$l.gold.max.LP || 'null' %], 
                    [% beams.$b.$l.gold.max.LR || 'null' %], 
                    [% beams.$b.$l.gold.max.LF || 'null' %], 
                    [% beams.$b.$l.gold.max.UP || 'null' %], 
                    [% beams.$b.$l.gold.max.UR || 'null' %], 
                    [% beams.$b.$l.gold.max.UF || 'null' %], 
		    [% beams.$b.$l.gold.maxiter || 'null' %], 
		    [% beams.$b.$l.gold.iter.size || 'null' %], 

		    [% info.$l.train %],
		    [% info.$l.dev %],
                    '[%- hosts.$b.$l.gold -%]',
                    '<span title="[% beams.$b.$l.gold.dir %]">[% beams.$b.$l.gold.timestamp %]</span>',
		 ]);
[% END %]

    var formatter = new google.visualization.ColorFormat();
//    formatter.addGradientRange(80,95,'white','#C5DEEA','#8ABBD7');
    formatter.addGradientRange(80,95,'white','#7BB77E','#37B73D');
    formatter.addGradientRange(70,80,'white','#F48404','#F2D9AE');
    formatter.format(data,1);
    formatter.format(data,2);
    formatter.format(data,3);
    formatter.format(data,4);
    formatter.format(data,5);
    formatter.format(data,6);
    formatter.format(data,11);

    var formatter = new google.visualization.NumberFormat({fractionDigits: 2});
    formatter.format(data,1);
    formatter.format(data,2);
    formatter.format(data,3);
    formatter.format(data,4);
    formatter.format(data,5);
    formatter.format(data,6);
    formatter.format(data,11);

      chart.draw(data, {width: '60em', allowHtml: true});
[% END %]

[% FOREACH l IN langs.keys.sort %]
      var data = new google.visualization.DataTable();

      var chart = new google.visualization.LineChart(document.getElementById('chart_lang_[%- l %]'));
      data.addColumn('string', 'beam');
      data.addColumn('number', 'LF');
      data.addColumn('number', 'LP');
      data.addColumn('number', 'LR');
[% FOREACH b IN langs.$l.keys.nsort %]
     data.addRow([ '[% b %]', 
                   [% langs.$l.$b.gold.max.LF || 'null' %], 
                   [% langs.$l.$b.gold.max.LP || 'null' %], 
                   [% langs.$l.$b.gold.max.LR || 'null' %], 
     ]);
[% END %]
      chart.draw(data, {width: 1200, height: 800, pointSize: 3, vAxis: {title: 'Labelled Scores', gridlines: {count: -1}, minorGridlines: {count: 1}}, hAxis: {title: 'beam size'}});
[% END %]

[% FOREACH l IN langs.keys.sort %]
      var data = new google.visualization.DataTable();

      var chart = new google.visualization.LineChart(document.getElementById('chart_iter_[%- l %]_LF'));
      data.addColumn('string', 'iter');
[% FOREACH b IN langs.$l.keys.nsort %]
      data.addColumn('number', 'LF [% b %]');
[% END %]
[% FOREACH i IN iters %]
      data.addRow([ '[%- i -%]'
 [%- FOREACH b IN langs.$l.keys.nsort %]
      , [% langs.$l.$b.gold.iter.$i.LF || 'null' %]
 [%- END %]
                 ]);
[% END %]
      chart.draw(data, {width: 1200, height: 800, pointSize: 3, vAxis: {title: 'Labelled Scores', gridlines: {count: -1}, minorGridlines: {count: 1}}, hAxis: {title: 'iteration'}});

      var data = new google.visualization.DataTable();

      var chart = new google.visualization.LineChart(document.getElementById('chart_iter_[%- l %]_LP'));
      data.addColumn('string', 'iter');
[% FOREACH b IN langs.$l.keys.nsort %]
      data.addColumn('number', 'LP [% b %]');
[% END %]
[% FOREACH i IN iters %]
      data.addRow([ '[%- i -%]'
 [%- FOREACH b IN langs.$l.keys.nsort %]
      , [% langs.$l.$b.gold.iter.$i.LP || 'null' %]
 [%- END %]
                 ]);
[% END %]
      chart.draw(data, {width: 1200, height: 800, pointSize: 3, vAxis: {title: 'Labelled Scores', gridlines: {count: -1}, minorGridlines: {count: 1}}, hAxis: {title: 'iteration'}});

      var data = new google.visualization.DataTable();

      var chart = new google.visualization.LineChart(document.getElementById('chart_iter_[%- l %]_LR'));
      data.addColumn('string', 'iter');
[% FOREACH b IN langs.$l.keys.nsort %]
      data.addColumn('number', 'LR [% b %]');
[% END %]
[% FOREACH i IN iters %]
      data.addRow([ '[%- i -%]'
 [%- FOREACH b IN langs.$l.keys.nsort %]
      , [% langs.$l.$b.gold.iter.$i.LR || 'null' %]
 [%- END %]
                 ]);
[% END %]
      chart.draw(data, {width: 1200, height: 800, pointSize: 3, vAxis: {title: 'Labelled Scores', gridlines: {count: -1}, minorGridlines: {count: 1}}, hAxis: {title: 'iteration'}});
[% END %]

    }


 </script>
  </head>

  <body>
    <h1>SemEval charts ([% date.format(date.now, "%y/%m/%d %H:%M:%S") %], dyalog-sr)</h1>

    <div id="tabs">
      <ul>
        <li><a href="#tabs-1">by languages</a></li>
        <li><a href="#tabs-2">by beam size</a></li>
        <li><a href="#tabs-3">by iteration</a></li>
        <li><a href="#tabs-4">synthesis</a></li>
      </ul>

      <div id="tabs-1">
      <ul>
[% FOREACH b IN beams.keys.nsort %]
           <li><a href="#tabs-1-[%- b %]">beam [%- b %]</a></li>
[% END %]
      </ul>
[% FOREACH b IN beams.keys.sort %]
    <div id="tabs-1-[%- b %]">
     <div id="chart_beam_[%- b %]"></div>
    </div>
[% END %]
     </div>

    <div id="tabs-2">
      <ul>
[% FOREACH l IN langs.keys.sort %]
           <li><a href="#tabs-2-[%- l %]">[%- info.$l.lang %]</a></li>
[% END %]
      </ul>

[% FOREACH l IN langs.keys %]
    <div id="tabs-2-[%- l %]">
      <div id="chart_lang_[%- l %]"></div>
    </div>
[% END %]
    </div>


    <div id="tabs-3">
      <ul>
[% FOREACH l IN langs.keys %]
           <li><a href="#tabs-3-[%- l %]_LF">[%- info.$l.lang %] LF</a></li>
           <li><a href="#tabs-3-[%- l %]_LP">[%- info.$l.lang %] LP</a></li>
           <li><a href="#tabs-3-[%- l %]_LR">[%- info.$l.lang %] LR</a></li>
[% END %]
      </ul>
[% FOREACH l IN langs.keys %]
    <div id="tabs-3-[%- l %]_LF">
     <div id="chart_iter_[%- l %]_LF"></div>
    </div>

    <div id="tabs-3-[%- l %]_LP">
     <div id="chart_iter_[%- l %]_LP"></div>
    </div>

    <div id="tabs-3-[%- l %]_LR">
     <div id="chart_iter_[%- l %]_LR"></div>
    </div>
[% END %]

    </div>

    <div id="tabs-4">
       <div id="chart_best"></div>
    </div>

   </div>

  </body>

<html>



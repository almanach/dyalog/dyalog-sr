#!/usr/bin/env perl

## train dpsr_parser in FTB6 on pred data

use strict;
use AppConfig qw/:argcount :expand/;
use POSIX ":sys_wait_h";

my %tasks = (
	     closed => {
			pcedt => {
				  #		 train => 'pcedt.train.planar.empty.sagae',
				  train => 'pcedt.train.sagae',
				  dev => 'pcedt.sec20.dev.sagae',
				  tagset => 'semeval_pcedt_planar_empty.tagset'
				 },
			dm => {
			       #		 train => 'dm.train.planar.empty.sagae',
			       train => 'dm.train.sagae',
			       dev => 'dm.sec20.dev.sagae',
			       tagset => 'semeval_dm_planar_empty.tagset'
			      },
			pas => {
				#		 train => 'pas.train.planar.empty.sagae',
				train => 'pas.train.sagae',
				dev => 'pas.sec20.dev.sagae',
				tagset => 'semeval_pas_planar_empty.tagset'
			       }
		       },
	     open => {
			pcedt => {
				  train => 'pcedt.train.none.morph_synt+bky+dist.sagae',
				  dev => 'pcedt.sec20.dev.none.morph_synt+bky+dist.conll',
				  tagset => 'semeval_pcedt_open.tagset'
				 },
			dm => {
			       train => 'dm.train.none.morph_synt+bky+dist.sagae',
			       dev => 'dm.sec20.dev.none.morph_synt+bky+dist.conll',
			       tagset => 'semeval_dm_open.tagset'
			      },
			pas => {
				train => 'pas.train.none.morph_synt+bky+dist.sagae',
				dev => 'pas.sec20.dev.none.morph_synt+bky+dist.sagae',
				tagset => 'semeval_pas_open.tagset'
			       }
		     },

	     );

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "lang=s@" => {DEFAULT => []},
                            "mode=s@" => {DEFAULT => []},
                            "beam=d@" => {DEFAULT => []},
                            "iter=d" => {DEFAULT => 10},
			    'evalcmd=f' => {DEFAULT => "/pinotdata/SemEval14_writable/eval-toolkit/dist/sdp.jar"},
			    'cdir=f' => {DEFAULT => "/pinotdata/SemEval14_writable/data"},
			    'mdir=f' => {DEFAULT => "/home/alpage/clergeri/scratch/semeval"},
			    'run!' => {DEFAULT => 0},
			    'start=d' => {DEFAULT => "n01"},
			    'sbeam=d' => {DEFAULT => 3 },
			    'task=s' => {DEFAULT => 'pcedt'},
			    'track=s' => {DEFAULT => 'closed'},
			    'random_skip=d' => {DEFAULT => 0 },
			   );

$config->args();

my $run = $config->run;

my $task = $config->task;
my $track = $config->track;

exists $tasks{$track}{$task} or die "missing task $task for track $track";

my $train = $tasks{$track}{$task}{train};
my $dev = $tasks{$track}{$task}{dev};
my $tagset = $tasks{$track}{$task}{tagset};

my $mdir = $config->mdir;
$run and mkdir $mdir;

my $evalcmd = $config->evalcmd;
my $cdir = $config->cdir;
my $iter = $config->iter;
my $sbeam = $config->sbeam;
my $skip = $config->random_skip;

my @beams = @{$config->beam};
@beams or push(@beams,1);

my @nodes = ( "n01" .. "n016"  );

my @save = @nodes;

my $current = $config->start;
$current or $current ||= $nodes[0];

until (grep {$current eq $_} @nodes) {
    $current++;
}

until ($current eq $nodes[0]) {
    shift @nodes;
}

my @pids=();

foreach my $beam (@beams) {
    print "Process SemEval beam=$beam task=$task track=$track\n";
    unless (@nodes) {
	@nodes = @save;
    }
    my $dir = "$mdir/${task}.beam${beam}";
    if (-d $dir && -f "$dir/train.log") {
	print "cleaning existing result dir\n";
	open(LOG,"<","$dir/train.log") || die "can't open log file: $!";
	my $iter=0;
	while(<LOG>) {
	    /:\s+iteration\s+(\d+)$/ and $iter=$1;
	    /:\s+done/ and $iter=0;
	}
	if ($iter) {
	    # clean last iteration
	    foreach my $file (glob("$dir/*.b*i$iter")) {
		print "\tremove $file\n";
		$run and unlink $file;
	    }
	}
	close(LOG);
    }
    my $node = shift @nodes;
    print "\trunning on node '$node'\n";
    my $cmd = "cd /home/alpage/clergeri/build/src/dyalog-sr; ./train.semeval.pl --iter=$iter --cdir=$cdir --evalcmd=$evalcmd --mdir=$dir --beam=$beam --sbeam $sbeam --train $train --dev $dev --tagset $tagset --random_skip=$skip";
    $run or print "\trunning cmd <$cmd>\n";
    if ($run) {
	if (my $pid = fork) {
	    push(@pids,$pid);
	} else {
	    exec($cmd);
	}
    } else {
    }
}

my $kid;
do {
    $kid = waitpid(-1,WNOHANG);
} while ($kid > 0);
